let Sequelize = require('sequelize');
let config = require('config');

let sequelize = new Sequelize(config.DB);

module.exports = sequelize;