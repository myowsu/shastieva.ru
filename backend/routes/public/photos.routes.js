const express = require('express');

let router = express.Router({
    mergeParams: true
});

router.get('/', (req, res, next) => {
    let queryParams = req.query;

    return req.auth_user.services.photosService.findAll({
        where: queryParams,
        order: [
            ['id', 'DESC']
        ]
    })
        .then(list => {
            return res.json({
                result: list,
                message: 'ok'
            })
        })
        .catch(next);
});

router.get('/:id(\\d+)', (req, res, next) => {
    const photoId = req.params.id;

    return req.auth_user.services.photosService.showPhoto(
        photoId,
    )
        .then(showPhotoObj => {
            return res.json({
                result: showPhotoObj,
                message: 'photo provided'
            });
        })
        .catch(next);
});

module.exports = router;
