const express = require('express');
const router = express.Router({
    mergeParams: true
});

router.get('/', (req, res, next) => {
    const userId = req.auth_user.user.id;

    return req.auth_user.services.photosService.findAll({
        where: {
            user_id: userId
        },
        order: [
            ['created_at', 'DESC']
        ]
    })
        .then(list => {
            return res.json({
                result: list,
                message: 'ok'
            })
        })
        .catch(next);
});

router.post('/', (req, res, next) => {
    const photoData = req.body;
    const file = req.files.file;

    if (!file) {
        return next(new Error('No file submitted'));
    }

    return req.auth_user.services.photosService.create(
        photoData,
        file,
        req.auth_user.user.id
    )
        .then(photo => {
            return res.json({
                result: photo,
                message: 'photo created'
            });
        })
        .catch(next);
});

router.put('/:id(\\d+)', (req, res, next) => {
    const photoId = req.params.id;
    const photoData = req.body;

    const file = req.files ? req.files.file : null;

    return req.auth_user.services.photosService.updatePhoto(
        photoId,
        photoData,
        file
    )
        .then(photo => {
            return res.json({
                result: photo,
                message: 'photo updated'
            });
        })
        .catch(next);
});

router.delete('/:id(\\d+)', (req, res, next) => {
    const photoId = req.params.id;

    return req.auth_user.services.photosService.deletePhoto(
        photoId
    )
        .then(() => {
            return res.json({
                result: true,
                message: 'photo deleted'
            })
        })
        .catch(next);
});

module.exports = router;