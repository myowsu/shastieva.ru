let express = require('express');

let servicesInjector = require('../../middlewares/services.injector.middleware');
let PhotosService = require.main.require('./services/photos.service');

let router = express.Router({
    mergeParams: true
});

router.use(require('../../middlewares/auth.middleware'));

router.use(
    (req, res, next) => servicesInjector(req, res, next, [
        PhotosService
    ])
);

router.use('/photos', require('./photos.routes'));

module.exports = router;