let express = require('express');
let passwordHash = require('password-hash');

let AuthService = require.main.require('./services/auth.service');

let User = require.main.require('./models/user');

let router = express.Router({
    mergeParams: true
});

module.exports = function () {
    router.post('/login', (req, res, next) => {
        let data = req.body;

        let email = data.email;
        let password = data.password;

        if (!email) {
            return next(new Error('no email provided'));
        }

        if (!password) {
            return next(new Error('no password provided'));
        }

        return User.findOne({
            where: {
                email: email
            }
        })
            .then(instance => {
                if (!instance) {
                    throw new Error(`Wrong email or password`);
                }

                let user = instance.get();

                if (!passwordHash.verify(password, user.password)) {
                    throw new Error('Wrong email or password');
                }

                let token = AuthService.generateAuthToken(user);

                res.cookie('auth-token', token);

                return res.json({
                    message: 'ok',
                    result: true
                });
            })
            .catch(next);
    });

    router.get('/me', (req, res, next) => {
        let token = req.cookies['auth-token'];

        if (!token) {
            return res.status(403).json({
                message: 'Failed to authenticate.',
            });
        }

        return AuthService.verifyAuthToken(token)
            .then(decodedUser => {
                let userId = decodedUser.id;

                return User.findOne({
                    where: {
                        id: userId
                    }
                })
            })
            .then(instance => {
                if (!instance) {
                    throw new Error(`No user found`);
                }

                let user = instance.get();

                // sanitizing user obj
                delete user.password;
                delete user.created_at;
                delete user.updated_at;
                delete user.deleted_at;

                return res.json({
                    message: 'ok',
                    result: user
                });
            })
            .catch(next);
    });

    router.delete('/logout', (req, res, next) => {
        res.clearCookie('auth-token');

        return res.json({
            message: 'ok',
            result: true
        });
    });

    return router;
};