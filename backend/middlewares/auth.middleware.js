let AuthService = require.main.require('./services/auth.service');

let User = require.main.require('./models/user');

module.exports = (req, res, next) => {
    if (req.method === 'OPTIONS') {
        return next();
    }

    if (!req.auth_user) {
        req.auth_user = {};
    }

    req.auth_user.user = {};

    let token = req.cookies['auth-token'];

    if (!token) {
        return next();
    }

    return AuthService.verifyAuthToken(token)
        .then(decodedUser => {
            let userId = decodedUser.id;

            return User.findOne({
                where: {
                    id: userId
                }
            })
        })
        .then(instance => {
            if (!instance) {
                return next();
            }

            let user = instance.get();

            // sanitizing user obj
            delete user.password;
            delete user.created_at;
            delete user.updated_at;
            delete user.deleted_at;

            req.auth_user.user = user;

            return next();
        })
        .catch(next);
};