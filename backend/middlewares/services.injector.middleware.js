let ServicesInjector = (req, res, next, services = []) => {
    if (req.method === 'OPTIONS') {
        return next();
    }

    if (!req.auth_user) {
        req.auth_user = {};
    }

    if (!req.auth_user.service) {
        req.auth_user.services = {};
    }

    let promises = [];

    services.forEach(serviceClass => {
        let serviceObject = new serviceClass();

        promises.push(
            serviceObject.initialize(req.auth_user)
                .then(() => {
                    const className = serviceObject.constructor.name;
                    const loweredFirstCharName = className.charAt(0).toLowerCase() + className.slice(1);

                    req.auth_user.services[loweredFirstCharName] = serviceObject;
                })
        );
    });

    return Promise.all(promises)
        .then(() => next())
        .catch(next);
};

module.exports = ServicesInjector;