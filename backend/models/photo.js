let DataTypes = require('sequelize');
let sequelize = require('../db/sequelize-db-connect');

let User = require('./user');

let Photo = sequelize.define('photos', {
        id: {
            type: DataTypes.INTEGER.UNSIGNED,
            autoIncrement: true,
            primaryKey: true
        },
        name: {
            type: DataTypes.STRING
        },
        description: {
            type: DataTypes.STRING
        },
        category: {
            type: DataTypes.STRING
        },
        filename: {
            type: DataTypes.STRING
        }
    }
);

Photo.belongsTo(User, {foreignKey: 'user_id', targetKey: 'id'});

module.exports = Photo;


