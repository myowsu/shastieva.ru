let Sequelize = require('sequelize');
let sequelize = require('../db/sequelize-db-connect');

let passwordHash = require('password-hash');

let User = sequelize.define('users', {
        id: {
            type: Sequelize.INTEGER.UNSIGNED,
            autoIncrement: true,
            primaryKey: true
        },
        password: {
            type: Sequelize.STRING,
            set: function (val) {
                this.setDataValue('password', passwordHash.generate(val));
            }
        },
        email: {
            type: Sequelize.STRING,
            unique: {
                msg: 'Email must be unique.',
            },
            validate: {
                isEmail: true,
            }
        },
    }
);

module.exports = User;