const fs = require('fs');
const sharp = require('sharp');
const randomHash = require('random-hash');

const BaseService = require('./base.service');
const Photo = require('../models/photo');

const LARGE_PHOTO_PREFIX = 'large-';
const SMALL_PHOTO_PREFIX = 'small-';

class PhotosService extends BaseService {

    /**
     * Uploads photo to storage resizing it.
     *
     * @param buffer
     * @param width
     * @param height
     * @param fName
     * @param format
     * @return {Promise}
     */
    uploadPhoto(buffer, width, height, fName, format = 'png') {
        return new Promise((resolve, reject) => {
            sharp(buffer)
                .resize(width, height)
                .embed()
                .toFormat(sharp.format[format])
                .toFile('./uploads/' + fName + `.${format}`, (err, info) => {
                    if (err) {
                        return reject(err);
                    }

                    return resolve(info);
                })
        });
    }

    /**
     * @todo move to base service methods like create/update/delete?
     */
    create(data, file, user_id) {
        const uniqueFName = Date.now() + randomHash.generateHash();
        const format = 'png';

        return Promise.all([
            this.uploadPhoto(file.data, 1200, null, LARGE_PHOTO_PREFIX + uniqueFName, format),
            this.uploadPhoto(file.data, 500, null, SMALL_PHOTO_PREFIX + uniqueFName, format)
        ])
            .then(() => {
                return Photo.create({
                    name: data.name,
                    filename: uniqueFName + '.' + format,
                    description: data.description || null,
                    category: data.category,
                    user_id: user_id
                })
            });
    }

    updatePhoto(id, data, file) {
        let dataToUpdate = data;
        let promises = [];

        const uniqueFName = Date.now() + randomHash.generateHash();
        const format = 'png';

        if (file) {
            promises.push(
                this.uploadPhoto(file.data, 1200, null, LARGE_PHOTO_PREFIX + uniqueFName, format),
                this.uploadPhoto(file.data, 500, null, SMALL_PHOTO_PREFIX + uniqueFName, format)
            );
        }

        let _oldFilename, _instanceUpdated;

        return Promise.all(promises)
            .then(() => {
                return Photo.findById(id);
            })
            .then(photoInstance => {
                if (!photoInstance) {
                    throw new Error(`Photo #${id} not found`);
                }

                _oldFilename = photoInstance.get().filename;

                if (file) {
                    dataToUpdate.filename = uniqueFName + '.' + format;
                }

                return photoInstance.update(
                    dataToUpdate
                );
            })
            .then(instanceUpdated => {
                _instanceUpdated = instanceUpdated;

                let promises = [];

                if (file) {
                    promises.push(
                        this.removeFile(LARGE_PHOTO_PREFIX + _oldFilename),
                        this.removeFile(SMALL_PHOTO_PREFIX + _oldFilename)
                    );
                }

                return Promise.all(promises);
            })
            .then(() => {
                return _instanceUpdated;
            });
    }

    findAll(params) {
        return Photo.findAll(params);
    }

    findById(id) {
        return Photo.findById(id);
    }

    showPhoto(photoId) {
        let _photo;

        return this.findById(photoId)
            .then(photo => {
                if (!photo) {
                    throw new Error(`Photo #${photoId} not found`);
                }

                _photo = photo;

                return Promise.all([
                    Photo.findOne({
                        where: {
                            category: photo.category,
                            id: {
                                $gt: photoId
                            }
                        },
                        attributes: ['id'],
                        limit: 1,
                        order: [
                            ['id', 'ASC']
                        ]
                    }),
                    Photo.findOne({
                        where: {
                            category: photo.category,
                            id: {
                                $lt: photoId
                            }
                        },
                        attributes: ['id'],
                        limit: 1,
                        order: [
                            ['id', 'DESC']
                        ]
                    }),
                ])
            })
            .then(results => {
                const nextPhotoInCategory = results[0];
                const prevPhotoInCategory = results[1];

                return {
                    photo: _photo,
                    next_id: nextPhotoInCategory? nextPhotoInCategory.id : null,
                    prev_id: prevPhotoInCategory? prevPhotoInCategory.id : null
                }
            });
    }

    removeFile(filePath, rootPath = './uploads/') {
        return new Promise((resolve, reject) => {
            fs.unlink(rootPath + filePath, (err, result) => {
                if (err) {
                    return reject(err);
                }

                return resolve(result);
            });
        });
    }

    deletePhoto(id) {
        return Photo.findOne({
            where: {
                id: id
            }
        })
            .then(photoInstance => {
                if (!photoInstance) {
                    throw new Error(`Photo #${id} not found`);
                }

                let photo = photoInstance.get();

                return Promise.all([
                    this.removeFile(LARGE_PHOTO_PREFIX + photo.filename),
                    this.removeFile(SMALL_PHOTO_PREFIX + photo.filename)
                ]);
            })
            .then(() => {
                return Photo.destroy({
                    where: {
                        id: id
                    }
                });
            })
            .then(() => {
                return true;
            })
    }
}

module.exports = PhotosService;