let jwt = require('jsonwebtoken');
let config = require('config');

class AuthService {
    static generateAuthToken(userObj) {
        let token = jwt.sign(userObj, config.jwt.secret, {
            expiresIn: 60 * 60 * 24
        });

        return token;
    }

    static verifyAuthToken(tokenStr) {
        return new Promise((resolve, reject) => {
            jwt.verify(tokenStr, config.jwt.secret, function (err, userObj) {
                if (err) {
                    return reject(err);
                }

                return resolve(userObj);
            });
        });
    }
}

module.exports = AuthService;