let colors = require('colors');

module.exports = function (app) {

    app.use((err, req, res, next) => {
        console.log(err);

        return next(err);
    });

    app.use((err, req, res, next) => {
        return res.status(500).json({
            message: err.message,
            code: err.code || 0
        });
    });

    app.all('*', (req, res, next) => {
        return res.status(404).json({
            message: 'Not found'
        });
    });

    process.on('exit', function (code) {
        console.log('About to exit with code:' + code);
    });

    app.on('error', function (err) {
        console.log('app.on error handler');
        console.log(err);
    });

    process.on('uncaughtException', function (err) {
        console.log('uncaughtException handler');
        console.log(err);
    });
};