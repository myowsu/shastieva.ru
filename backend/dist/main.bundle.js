webpackJsonp([1,4],Array(28).concat([
/* 28 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_requesters_http_service_requester__ = __webpack_require__(30);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AuthService = (function () {
    function AuthService(requester) {
        this.requester = requester;
    }
    AuthService.prototype.getAuthUser = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (_this.authUser) {
                return resolve(_this.authUser);
            }
            return _this.getMe()
                .then(function (authUser) {
                _this.authUser = authUser;
                return resolve(_this.authUser);
            })
                .catch(reject);
        });
    };
    AuthService.prototype.getMe = function () {
        return this.requester.makeRequest('/auth/me', 'GET');
    };
    AuthService.prototype.login = function (email, password) {
        return this.requester.makeRequest('/auth/login', 'POST', {
            email: email,
            password: password
        });
    };
    AuthService.prototype.logout = function () {
        var _this = this;
        return this.requester.makeRequest('/auth/logout', 'DELETE')
            .then(function () {
            _this.authUser = null;
            return true;
        });
    };
    AuthService = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__shared_requesters_http_service_requester__["a" /* HttpServiceRequester */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__shared_requesters_http_service_requester__["a" /* HttpServiceRequester */]) === "function" && _a || Object])
    ], AuthService);
    return AuthService;
    var _a;
}());

//# sourceMappingURL=auth.service.js.map

/***/ }),
/* 29 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Photo; });
/* unused harmony export ShowPhotoObject */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return CATEGORIES_LIST; });
var Photo = (function () {
    function Photo() {
    }
    return Photo;
}());

var ShowPhotoObject = (function () {
    function ShowPhotoObject() {
    }
    return ShowPhotoObject;
}());

var CATEGORIES_LIST = [
    'bw',
    'colour',
    'mobile'
];
//# sourceMappingURL=photo.model.js.map

/***/ }),
/* 30 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core___ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise__ = __webpack_require__(242);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__environments_environment__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__errors_unauthorized_request_error__ = __webpack_require__(146);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular4_notify__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular4_notify___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_angular4_notify__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HttpServiceRequester; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var HttpServiceRequester = (function () {
    function HttpServiceRequester(http, router, notificationsService) {
        this.http = http;
        this.router = router;
        this.notificationsService = notificationsService;
        this.baseUrl = __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].baseUrl;
    }
    HttpServiceRequester.prototype.makeRequest = function (action, method, data) {
        var _this = this;
        if (data === void 0) { data = {}; }
        var params, body;
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Headers */]({
            'Content-Type': 'application/json',
        });
        console.log('make http call', action, data);
        if (method === 'GET' || method === 'DELETE') {
            params = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["c" /* URLSearchParams */]();
            var _loop_1 = function (prop) {
                if (data.hasOwnProperty(prop)) {
                    var value = data[prop];
                    if (Array.isArray(value)) {
                        value.forEach(function (val) {
                            params.append(prop + '[]', val);
                        });
                    }
                    else {
                        params.set(prop, data[prop]);
                    }
                }
            };
            for (var prop in data) {
                _loop_1(prop);
            }
        }
        else if (['POST', 'PUT', 'PATCH'].indexOf(method) !== -1) {
            body = JSON.stringify(data);
        }
        var requestOptions = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers,
            method: method,
            search: params ? params : null,
            body: body ? body : null,
            withCredentials: true
        });
        return this.http.request(this.baseUrl + action, requestOptions)
            .toPromise()
            .then(function (response) {
            // todo check if 202 code is really needed
            if (response.status === 202) {
                return _this.handleError(response);
            }
            var resObj = response.json();
            return resObj.result;
        })
            .catch(function (response) {
            return _this.handleError(response);
        });
    };
    HttpServiceRequester.prototype.submitFormData = function (action, method, data) {
        var _this = this;
        return this.http.request(this.baseUrl + action, {
            method: method,
            withCredentials: true,
            body: data,
        })
            .toPromise()
            .then(function (response) {
            // todo check if 202 code is really needed
            if (response.status === 202) {
                return _this.handleError(response);
            }
            var resObj = response.json();
            return resObj.result;
        })
            .catch(function (response) {
            return _this.handleError(response);
        });
    };
    HttpServiceRequester.prototype.handleError = function (response) {
        var message = response.statusText;
        var errorCode;
        if (response['_body']) {
            try {
                var jsonBody = JSON.parse(response['_body']);
                if (jsonBody.message) {
                    message = jsonBody.message;
                }
                if (jsonBody.result) {
                    errorCode = jsonBody.code;
                }
            }
            catch (err) {
                message = 'Error while parsing response server. Service unavailable.';
            }
        }
        if (response.status === 403) {
            throw new __WEBPACK_IMPORTED_MODULE_5__errors_unauthorized_request_error__["a" /* UnauthorizedRequestError */]('You are not authorized to perform this request');
        }
        this.notificationsService.addError(message);
        throw new Error(message);
    };
    HttpServiceRequester = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core___["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_http__["e" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_http__["e" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["Router"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["Router"]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_6_angular4_notify__["NotificationsService"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6_angular4_notify__["NotificationsService"]) === "function" && _c || Object])
    ], HttpServiceRequester);
    return HttpServiceRequester;
    var _a, _b, _c;
}());

//# sourceMappingURL=http.service.requester.js.map

/***/ }),
/* 31 */,
/* 32 */,
/* 33 */,
/* 34 */,
/* 35 */,
/* 36 */,
/* 37 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_requesters_http_service_requester__ = __webpack_require__(30);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PhotosService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PhotosService = (function () {
    function PhotosService(requester) {
        this.requester = requester;
    }
    PhotosService.prototype.list = function () {
        return this.requester.makeRequest('/admin/photos', 'GET');
    };
    PhotosService.prototype.createPhoto = function (data) {
        return this.requester.submitFormData('/admin/photos/', 'POST', data);
    };
    PhotosService.prototype.updatePhoto = function (photoId, data) {
        return this.requester.submitFormData('/admin/photos/' + photoId, 'PUT', data);
    };
    PhotosService.prototype.deletePhoto = function (id) {
        return this.requester.makeRequest('/admin/photos/' + id, 'DELETE');
    };
    PhotosService = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__shared_requesters_http_service_requester__["a" /* HttpServiceRequester */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__shared_requesters_http_service_requester__["a" /* HttpServiceRequester */]) === "function" && _a || Object])
    ], PhotosService);
    return PhotosService;
    var _a;
}());

//# sourceMappingURL=photos.service.js.map

/***/ }),
/* 38 */,
/* 39 */,
/* 40 */,
/* 41 */,
/* 42 */,
/* 43 */,
/* 44 */,
/* 45 */,
/* 46 */,
/* 47 */,
/* 48 */,
/* 49 */,
/* 50 */,
/* 51 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_photos_service__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_models_photo_model__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angular4_file_submit__ = __webpack_require__(126);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angular4_file_submit___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_angular4_file_submit__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angular4_notify__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angular4_notify___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_angular4_notify__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UploadPhotoComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var UploadPhotoComponent = (function () {
    function UploadPhotoComponent(dialogRef, notificationsService, photosService) {
        this.dialogRef = dialogRef;
        this.notificationsService = notificationsService;
        this.photosService = photosService;
        this.categoriesList = __WEBPACK_IMPORTED_MODULE_3__shared_models_photo_model__["b" /* CATEGORIES_LIST */];
        this.model = {
            name: '',
            description: '',
            category: '',
        };
    }
    UploadPhotoComponent.prototype.ngOnInit = function () {
        if (!this.existingPhoto) {
            return;
        }
        for (var prop in this.model) {
            if (this.model.hasOwnProperty(prop) && this.existingPhoto[prop]) {
                this.model[prop] = this.existingPhoto[prop];
            }
        }
    };
    UploadPhotoComponent.prototype.validateModel = function () {
        var _this = this;
        var isValid = true;
        var requiredPropsList = [
            'name',
            'category',
        ];
        requiredPropsList.forEach(function (requiredProp) {
            if (!_this.model[requiredProp]) {
                _this.notificationsService.addError(requiredProp + " is required");
                isValid = false;
                return false;
            }
        });
        return isValid;
    };
    UploadPhotoComponent.prototype.uploadPhoto = function () {
        var _this = this;
        if (!this.validateModel()) {
            return;
        }
        var formData = this.fileSubmitDirective.getFormData();
        for (var prop in this.model) {
            formData.set(prop, this.model[prop]);
        }
        var promises = [];
        if (!this.existingPhoto) {
            promises.push(this.photosService.createPhoto(formData));
        }
        else {
            promises.push(this.photosService.updatePhoto(this.existingPhoto.id, formData));
        }
        return Promise.all(promises)
            .then(function (results) {
            _this.dialogRef.close(results[0]);
        });
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__shared_models_photo_model__["a" /* Photo */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__shared_models_photo_model__["a" /* Photo */]) === "function" && _a || Object)
    ], UploadPhotoComponent.prototype, "existingPhoto", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_4_angular4_file_submit__["FileSubmitDirective"]),
        __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4_angular4_file_submit__["FileSubmitDirective"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_angular4_file_submit__["FileSubmitDirective"]) === "function" && _b || Object)
    ], UploadPhotoComponent.prototype, "fileSubmitDirective", void 0);
    UploadPhotoComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-upload-photo',
            template: __webpack_require__(225),
            styles: [__webpack_require__(214)]
        }),
        __metadata("design:paramtypes", [typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_material__["c" /* MdDialogRef */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_material__["c" /* MdDialogRef */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_5_angular4_notify__["NotificationsService"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5_angular4_notify__["NotificationsService"]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2__services_photos_service__["a" /* PhotosService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_photos_service__["a" /* PhotosService */]) === "function" && _e || Object])
    ], UploadPhotoComponent);
    return UploadPhotoComponent;
    var _a, _b, _c, _d, _e;
}());

//# sourceMappingURL=upload-photo.component.js.map

/***/ }),
/* 52 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_requesters_http_service_requester__ = __webpack_require__(30);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PublicPhotosService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PublicPhotosService = (function () {
    function PublicPhotosService(requester) {
        this.requester = requester;
    }
    PublicPhotosService.prototype.list = function (category) {
        return this.requester.makeRequest('/public/photos', 'GET', {
            category: category
        });
    };
    PublicPhotosService.prototype.show = function (id) {
        return this.requester.makeRequest('/public/photos/' + id, 'GET');
    };
    PublicPhotosService = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__shared_requesters_http_service_requester__["a" /* HttpServiceRequester */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__shared_requesters_http_service_requester__["a" /* HttpServiceRequester */]) === "function" && _a || Object])
    ], PublicPhotosService);
    return PublicPhotosService;
    var _a;
}());

//# sourceMappingURL=public.photos.service.js.map

/***/ }),
/* 53 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var ErrorsNumbers = (function () {
    function ErrorsNumbers() {
    }
    ErrorsNumbers.UNAUTHORIZED_REQUEST_ERROR = 100;
    return ErrorsNumbers;
}());
/* harmony default export */ __webpack_exports__["a"] = ErrorsNumbers;
//# sourceMappingURL=errors-numbers.js.map

/***/ }),
/* 54 */,
/* 55 */,
/* 56 */,
/* 57 */,
/* 58 */,
/* 59 */,
/* 60 */,
/* 61 */,
/* 62 */,
/* 63 */,
/* 64 */,
/* 65 */,
/* 66 */,
/* 67 */,
/* 68 */,
/* 69 */,
/* 70 */,
/* 71 */,
/* 72 */,
/* 73 */,
/* 74 */,
/* 75 */,
/* 76 */,
/* 77 */,
/* 78 */,
/* 79 */,
/* 80 */,
/* 81 */,
/* 82 */,
/* 83 */,
/* 84 */,
/* 85 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_auth_service__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__(6);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AdminComponent = (function () {
    function AdminComponent(authService, router) {
        this.authService = authService;
        this.router = router;
        this.notificationsOptions = {
            pauseOnHover: true,
            clickToClose: true,
            timeOut: 5000
        };
    }
    AdminComponent.prototype.doLogout = function () {
        var _this = this;
        return this.authService.logout()
            .then(function () {
            console.log('router navigate');
            _this.router.navigate(['/admin', 'auth', 'login']);
            return true;
        });
    };
    AdminComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-admin',
            template: __webpack_require__(218),
            styles: [__webpack_require__(201)]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_auth_service__["a" /* AuthService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["Router"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["Router"]) === "function" && _b || Object])
    ], AdminComponent);
    return AdminComponent;
    var _a, _b;
}());

//# sourceMappingURL=admin.component.js.map

/***/ }),
/* 86 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_auth_service__ = __webpack_require__(28);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LoginComponent = (function () {
    function LoginComponent(authService, router) {
        this.authService = authService;
        this.router = router;
        this.model = {
            email: '',
            password: ''
        };
    }
    LoginComponent.prototype.login = function () {
        var _this = this;
        return this.authService.login(this.model.email, this.model.password)
            .then(function () {
            return _this.router.navigate(['/admin']);
        });
    };
    LoginComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-auth-login',
            template: __webpack_require__(219),
            styles: [__webpack_require__(202)]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__services_auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_auth_service__["a" /* AuthService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"]) === "function" && _b || Object])
    ], LoginComponent);
    return LoginComponent;
    var _a, _b;
}());

//# sourceMappingURL=login.component.js.map

/***/ }),
/* 87 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DashboardComponent = (function () {
    function DashboardComponent() {
    }
    DashboardComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-admin-dashboard',
            template: __webpack_require__(220),
            styles: [__webpack_require__(209)]
        }),
        __metadata("design:paramtypes", [])
    ], DashboardComponent);
    return DashboardComponent;
}());

//# sourceMappingURL=dashboard.component.js.map

/***/ }),
/* 88 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(6);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeneralComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var GeneralComponent = (function () {
    function GeneralComponent(router) {
        this.router = router;
    }
    GeneralComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-admin-general',
            template: __webpack_require__(221),
            styles: [__webpack_require__(210)]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"]) === "function" && _a || Object])
    ], GeneralComponent);
    return GeneralComponent;
    var _a;
}());

//# sourceMappingURL=general.component.js.map

/***/ }),
/* 89 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_photos_service__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_models_photo_model__ = __webpack_require__(29);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DeletePhotoComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DeletePhotoComponent = (function () {
    function DeletePhotoComponent(dialogRef, photosService) {
        this.dialogRef = dialogRef;
        this.photosService = photosService;
    }
    DeletePhotoComponent.prototype.deletePhoto = function () {
        var _this = this;
        return this.photosService.deletePhoto(this.photo.id)
            .then(function () {
            _this.dialogRef.close(_this.photo);
        });
    };
    DeletePhotoComponent.prototype.cancel = function () {
        return this.dialogRef.close(false);
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__shared_models_photo_model__["a" /* Photo */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__shared_models_photo_model__["a" /* Photo */]) === "function" && _a || Object)
    ], DeletePhotoComponent.prototype, "photo", void 0);
    DeletePhotoComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-delete-photo',
            template: __webpack_require__(222),
            styles: [__webpack_require__(211)]
        }),
        __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_material__["c" /* MdDialogRef */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_material__["c" /* MdDialogRef */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__services_photos_service__["a" /* PhotosService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_photos_service__["a" /* PhotosService */]) === "function" && _c || Object])
    ], DeletePhotoComponent);
    return DeletePhotoComponent;
    var _a, _b, _c;
}());

//# sourceMappingURL=delete-photo.component.js.map

/***/ }),
/* 90 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__upload_photo_upload_photo_component__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_photos_service__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_models_photo_model__ = __webpack_require__(29);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PhotosIndexComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PhotosIndexComponent = (function () {
    function PhotosIndexComponent(dialog, photosService) {
        this.dialog = dialog;
        this.photosService = photosService;
        this.categoriesList = __WEBPACK_IMPORTED_MODULE_4__shared_models_photo_model__["b" /* CATEGORIES_LIST */];
        this.filter = {
            category: __WEBPACK_IMPORTED_MODULE_4__shared_models_photo_model__["b" /* CATEGORIES_LIST */]
        };
    }
    PhotosIndexComponent.prototype.ngOnInit = function () {
        return this.getPhotosList();
    };
    PhotosIndexComponent.prototype.openUploadDialog = function () {
        var _this = this;
        var dialogRef = this.dialog.open(__WEBPACK_IMPORTED_MODULE_1__upload_photo_upload_photo_component__["a" /* UploadPhotoComponent */]);
        dialogRef.afterClosed()
            .subscribe(function (createdPhoto) {
            if (!createdPhoto) {
                return;
            }
            // add newly created photo to the top of the list
            _this.photos.unshift(createdPhoto);
            _this.filterPhotosList();
        });
    };
    PhotosIndexComponent.prototype.getPhotosList = function () {
        var _this = this;
        return this.photosService.list()
            .then(function (photos) {
            _this.photos = photos;
            _this.filterPhotosList();
        });
    };
    PhotosIndexComponent.prototype.onPhotoDeleted = function (photo) {
        var _this = this;
        this.photos.forEach(function (photoFromList, index) {
            if (photo.id === photoFromList.id) {
                _this.photos.splice(index, 1);
                _this.filterPhotosList();
            }
        });
    };
    PhotosIndexComponent.prototype.onPhotoUpdated = function (photo) {
        this.photos = this.photos.map(function (photoFromList, index) {
            if (photo.id === photoFromList.id) {
                return photo;
            }
            return photoFromList;
        });
        this.filterPhotosList();
    };
    PhotosIndexComponent.prototype.filterPhotosList = function () {
        var _this = this;
        this.filteredPhotos = [];
        this.filteredPhotos = this.photos.filter(function (photo) {
            var valid = true;
            if (_this.filter.category.indexOf(photo.category) === -1) {
                valid = false;
            }
            return valid;
        });
    };
    PhotosIndexComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-photos-index',
            template: __webpack_require__(223),
            styles: [__webpack_require__(212)]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_material__["d" /* MdDialog */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_material__["d" /* MdDialog */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__services_photos_service__["a" /* PhotosService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_photos_service__["a" /* PhotosService */]) === "function" && _b || Object])
    ], PhotosIndexComponent);
    return PhotosIndexComponent;
    var _a, _b;
}());

//# sourceMappingURL=index.component.js.map

/***/ }),
/* 91 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(6);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SettingsComponent = (function () {
    function SettingsComponent(router) {
        this.router = router;
    }
    SettingsComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-admin-settings',
            template: __webpack_require__(226),
            styles: [__webpack_require__(215)]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"]) === "function" && _a || Object])
    ], SettingsComponent);
    return SettingsComponent;
    var _a;
}());

//# sourceMappingURL=settings.component.js.map

/***/ }),
/* 92 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth_service__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__errors_errors_numbers__ = __webpack_require__(53);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthResolver; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





var AuthResolver = (function () {
    function AuthResolver(Http, authService, router) {
        this.Http = Http;
        this.authService = authService;
        this.router = router;
    }
    AuthResolver.prototype.resolve = function (route, state) {
        return this.load();
    };
    AuthResolver.prototype.load = function () {
        var _this = this;
        return this.authService.getAuthUser()
            .catch(function (err) {
            // todo find way to catch specific error types in typescript
            if (err.code === __WEBPACK_IMPORTED_MODULE_4__errors_errors_numbers__["a" /* default */].UNAUTHORIZED_REQUEST_ERROR) {
                _this.router.navigate(['/admin', 'auth', 'login']);
                return true;
            }
            throw err;
        });
    };
    AuthResolver = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __param(1, __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_3__auth_service__["a" /* AuthService */])),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["e" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["e" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__auth_service__["a" /* AuthService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"]) === "function" && _c || Object])
    ], AuthResolver);
    return AuthResolver;
    var _a, _b, _c;
}());

//# sourceMappingURL=auth.resolver.js.map

/***/ }),
/* 93 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth_service__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__errors_errors_numbers__ = __webpack_require__(53);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GuestResolver; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





var GuestResolver = (function () {
    function GuestResolver(Http, authService, router) {
        this.Http = Http;
        this.authService = authService;
        this.router = router;
    }
    GuestResolver.prototype.resolve = function (route, state) {
        return this.load();
    };
    GuestResolver.prototype.load = function () {
        var _this = this;
        return this.authService.getAuthUser()
            .then(function (user) {
            if (user) {
                _this.router.navigate(['/admin']);
            }
            return true;
        })
            .catch(function (err) {
            if (err.code === __WEBPACK_IMPORTED_MODULE_4__errors_errors_numbers__["a" /* default */].UNAUTHORIZED_REQUEST_ERROR) {
                // it's ok for quest resolving /me endpoint
                return true;
            }
            throw err;
        });
    };
    GuestResolver = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __param(1, __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_3__auth_service__["a" /* AuthService */])),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["e" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["e" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__auth_service__["a" /* AuthService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"]) === "function" && _c || Object])
    ], GuestResolver);
    return GuestResolver;
    var _a, _b, _c;
}());

//# sourceMappingURL=guest.resolver.js.map

/***/ }),
/* 94 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_public_photos_service__ = __webpack_require__(52);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CategoryComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CategoryComponent = (function () {
    function CategoryComponent(activatedRoute, publicPhotosService) {
        this.activatedRoute = activatedRoute;
        this.publicPhotosService = publicPhotosService;
        this.photos = [];
    }
    CategoryComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activatedRoute.params.subscribe(function (params) {
            _this.category = params['category'];
            return _this.getPhotos(_this.category);
        });
    };
    CategoryComponent.prototype.getPhotos = function (category) {
        var _this = this;
        return this.publicPhotosService.list(category)
            .then(function (photos) {
            _this.photos = photos;
        });
    };
    CategoryComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-category',
            template: __webpack_require__(228),
            styles: [__webpack_require__(204)]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["ActivatedRoute"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["ActivatedRoute"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_public_photos_service__["a" /* PublicPhotosService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_public_photos_service__["a" /* PublicPhotosService */]) === "function" && _b || Object])
    ], CategoryComponent);
    return CategoryComponent;
    var _a, _b;
}());

//# sourceMappingURL=category.component.js.map

/***/ }),
/* 95 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_public_photos_service__ = __webpack_require__(52);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PhotoDetailedComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PhotoDetailedComponent = (function () {
    function PhotoDetailedComponent(activatedRoute, publicPhotosService) {
        this.activatedRoute = activatedRoute;
        this.publicPhotosService = publicPhotosService;
    }
    PhotoDetailedComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activatedRoute.params.subscribe(function (params) {
            var id = +params['id'];
            return _this.getPhoto(id);
        });
    };
    PhotoDetailedComponent.prototype.getPhoto = function (id) {
        var _this = this;
        return this.publicPhotosService.show(id)
            .then(function (showPhotoObj) {
            _this.photo = showPhotoObj.photo;
            _this.showPhotoObj = showPhotoObj;
        });
    };
    PhotoDetailedComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-photo-detailed',
            template: __webpack_require__(229),
            styles: [__webpack_require__(205)]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["ActivatedRoute"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["ActivatedRoute"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_public_photos_service__["a" /* PublicPhotosService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_public_photos_service__["a" /* PublicPhotosService */]) === "function" && _b || Object])
    ], PhotoDetailedComponent);
    return PhotoDetailedComponent;
    var _a, _b;
}());

//# sourceMappingURL=photo-detailed.component.js.map

/***/ }),
/* 96 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GreetingsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var GreetingsComponent = (function () {
    function GreetingsComponent() {
    }
    GreetingsComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-greetings',
            template: __webpack_require__(231),
            styles: [__webpack_require__(207)]
        })
    ], GreetingsComponent);
    return GreetingsComponent;
}());

//# sourceMappingURL=greetings.component.js.map

/***/ }),
/* 97 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(6);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PublicComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PublicComponent = (function () {
    function PublicComponent(router) {
        this.router = router;
    }
    PublicComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-public',
            template: __webpack_require__(232),
            styles: [__webpack_require__(208)]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"]) === "function" && _a || Object])
    ], PublicComponent);
    return PublicComponent;
    var _a;
}());

//# sourceMappingURL=public.component.js.map

/***/ }),
/* 98 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
var environment = {
    production: true,
    baseUrl: 'http://shastieva.ru'
};
//# sourceMappingURL=environment.js.map

/***/ }),
/* 99 */,
/* 100 */,
/* 101 */,
/* 102 */,
/* 103 */,
/* 104 */,
/* 105 */,
/* 106 */,
/* 107 */,
/* 108 */,
/* 109 */,
/* 110 */,
/* 111 */,
/* 112 */,
/* 113 */,
/* 114 */,
/* 115 */,
/* 116 */,
/* 117 */,
/* 118 */,
/* 119 */,
/* 120 */,
/* 121 */,
/* 122 */,
/* 123 */,
/* 124 */,
/* 125 */,
/* 126 */
/***/ (function(module, exports, __webpack_require__) {

(function (global, factory) {
	 true ? factory(exports, __webpack_require__(0), __webpack_require__(20)) :
	typeof define === 'function' && define.amd ? define(['exports', '@angular/core', '@angular/common'], factory) :
	(factory((global['angular4-file-submit'] = {}),global._angular_core,global._angular_common));
}(this, (function (exports,_angular_core,_angular_common) { 'use strict';

var FileSubmitDirective = (function () {
    /**
     * @param {?} el
     */
    function FileSubmitDirective(el) {
        this.el = el;
        this.formData = new FormData();
    }
    /**
     * @param {?} e
     * @return {?}
     */
    FileSubmitDirective.prototype.onChange = function (e) {
        return this.setFileInput(e);
    };
    /**
     * @param {?} $event
     * @return {?}
     */
    FileSubmitDirective.prototype.setFileInput = function ($event) {
        var /** @type {?} */ file = $event.target.files[0];
        if (!file) {
            throw new Error('no file selected');
        }
        this.formData = new FormData();
        this.formData.append('file', file);
    };
    /**
     * @return {?}
     */
    FileSubmitDirective.prototype.getFormData = function () {
        return this.formData;
    };
    return FileSubmitDirective;
}());
FileSubmitDirective.decorators = [
    { type: _angular_core.Directive, args: [{
                selector: '[ng4-file-submit]'
            },] },
];
/**
 * @nocollapse
 */
FileSubmitDirective.ctorParameters = function () { return [
    { type: _angular_core.ElementRef, },
]; };
FileSubmitDirective.propDecorators = {
    'onChange': [{ type: _angular_core.HostListener, args: ['change', ['$event'],] },],
};

var FileSubmitModule = (function () {
    function FileSubmitModule() {
    }
    /**
     * @return {?}
     */
    FileSubmitModule.forRoot = function () {
        return {
            ngModule: FileSubmitModule,
            providers: []
        };
    };
    return FileSubmitModule;
}());
FileSubmitModule.decorators = [
    { type: _angular_core.NgModule, args: [{
                imports: [
                    _angular_common.CommonModule
                ],
                declarations: [
                    FileSubmitDirective,
                ],
                exports: [
                    FileSubmitDirective,
                ]
            },] },
];
/**
 * @nocollapse
 */
FileSubmitModule.ctorParameters = function () { return []; };

exports.FileSubmitModule = FileSubmitModule;
exports.FileSubmitDirective = FileSubmitDirective;

Object.defineProperty(exports, '__esModule', { value: true });

})));


/***/ }),
/* 127 */
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = 127;


/***/ }),
/* 128 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__(98);




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),
/* 129 */,
/* 130 */,
/* 131 */,
/* 132 */,
/* 133 */,
/* 134 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__admin_routing__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_material__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_auth_login_login_component__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_auth_service__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_hammerjs__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_hammerjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_hammerjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_resolvers_auth_resolver__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__shared_requesters_http_service_requester__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__admin_component__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__angular_platform_browser_animations__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__components_dashboard_photos_index_index_component__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__services_resolvers_guest_resolver__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__components_dashboard_settings_settings_component__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__shared_shared_module__ = __webpack_require__(144);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__components_dashboard_photos_photo_card_photo_card_component__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__components_dashboard_photos_upload_photo_upload_photo_component__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__services_photos_service__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__components_dashboard_photos_delete_photo_delete_photo_component__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22_app_admin_components_dashboard_dashboard_component__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__components_dashboard_general_general_component__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24_angular4_notify__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24_angular4_notify___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_24_angular4_notify__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25_angular4_file_submit__ = __webpack_require__(126);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25_angular4_file_submit___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_25_angular4_file_submit__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


























var AdminModule = (function () {
    function AdminModule() {
    }
    AdminModule = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_7__components_auth_login_login_component__["a" /* LoginComponent */],
                __WEBPACK_IMPORTED_MODULE_12__admin_component__["a" /* AdminComponent */],
                __WEBPACK_IMPORTED_MODULE_14__components_dashboard_photos_index_index_component__["a" /* PhotosIndexComponent */],
                __WEBPACK_IMPORTED_MODULE_18__components_dashboard_photos_photo_card_photo_card_component__["a" /* PhotoCardComponent */],
                __WEBPACK_IMPORTED_MODULE_19__components_dashboard_photos_upload_photo_upload_photo_component__["a" /* UploadPhotoComponent */],
                __WEBPACK_IMPORTED_MODULE_21__components_dashboard_photos_delete_photo_delete_photo_component__["a" /* DeletePhotoComponent */],
                __WEBPACK_IMPORTED_MODULE_22_app_admin_components_dashboard_dashboard_component__["a" /* DashboardComponent */],
                __WEBPACK_IMPORTED_MODULE_23__components_dashboard_general_general_component__["a" /* GeneralComponent */],
                __WEBPACK_IMPORTED_MODULE_16__components_dashboard_settings_settings_component__["a" /* SettingsComponent */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_13__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                // SimpleNotificationsModule,
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_router__["RouterModule"].forChild(__WEBPACK_IMPORTED_MODULE_5__admin_routing__["a" /* routes */]),
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["a" /* MaterialModule */],
                __WEBPACK_IMPORTED_MODULE_17__shared_shared_module__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_24_angular4_notify__["NotificationsModule"],
                __WEBPACK_IMPORTED_MODULE_25_angular4_file_submit__["FileSubmitModule"]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_8__services_auth_service__["a" /* AuthService */],
                __WEBPACK_IMPORTED_MODULE_10__services_resolvers_auth_resolver__["a" /* AuthResolver */],
                __WEBPACK_IMPORTED_MODULE_15__services_resolvers_guest_resolver__["a" /* GuestResolver */],
                __WEBPACK_IMPORTED_MODULE_11__shared_requesters_http_service_requester__["a" /* HttpServiceRequester */],
                __WEBPACK_IMPORTED_MODULE_20__services_photos_service__["a" /* PhotosService */],
                __WEBPACK_IMPORTED_MODULE_24_angular4_notify__["NotificationsService"]
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_19__components_dashboard_photos_upload_photo_upload_photo_component__["a" /* UploadPhotoComponent */],
                __WEBPACK_IMPORTED_MODULE_21__components_dashboard_photos_delete_photo_delete_photo_component__["a" /* DeletePhotoComponent */]
            ],
            bootstrap: []
        })
    ], AdminModule);
    return AdminModule;
}());

//# sourceMappingURL=admin.module.js.map

/***/ }),
/* 135 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__services_resolvers_auth_resolver__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_auth_login_login_component__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__admin_component__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_resolvers_guest_resolver__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_dashboard_dashboard_component__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_dashboard_photos_index_index_component__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_dashboard_general_general_component__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_dashboard_settings_settings_component__ = __webpack_require__(91);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return routes; });








var routes = [
    {
        path: 'admin',
        component: __WEBPACK_IMPORTED_MODULE_2__admin_component__["a" /* AdminComponent */],
        children: [
            {
                path: '',
                redirectTo: 'dashboard',
                pathMatch: 'full'
            },
            {
                path: 'auth',
                resolve: {
                    guest: __WEBPACK_IMPORTED_MODULE_3__services_resolvers_guest_resolver__["a" /* GuestResolver */]
                },
                children: [
                    {
                        path: 'login',
                        component: __WEBPACK_IMPORTED_MODULE_1__components_auth_login_login_component__["a" /* LoginComponent */]
                    },
                ]
            },
            {
                path: 'dashboard',
                resolve: {
                    auth: __WEBPACK_IMPORTED_MODULE_0__services_resolvers_auth_resolver__["a" /* AuthResolver */],
                },
                component: __WEBPACK_IMPORTED_MODULE_4__components_dashboard_dashboard_component__["a" /* DashboardComponent */],
                children: [
                    {
                        path: '',
                        redirectTo: 'general',
                        pathMatch: 'full'
                    },
                    {
                        path: 'photos',
                        component: __WEBPACK_IMPORTED_MODULE_5__components_dashboard_photos_index_index_component__["a" /* PhotosIndexComponent */]
                    },
                    {
                        path: 'general',
                        component: __WEBPACK_IMPORTED_MODULE_6__components_dashboard_general_general_component__["a" /* GeneralComponent */]
                    },
                    {
                        path: 'settings',
                        component: __WEBPACK_IMPORTED_MODULE_7__components_dashboard_settings_settings_component__["a" /* SettingsComponent */]
                    },
                ]
            }
        ]
    }
];
//# sourceMappingURL=admin.routing.js.map

/***/ }),
/* 136 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_models_photo_model__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__delete_photo_delete_photo_component__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__upload_photo_upload_photo_component__ = __webpack_require__(51);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PhotoCardComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PhotoCardComponent = (function () {
    function PhotoCardComponent(dialog) {
        this.dialog = dialog;
        this.photoDeleted = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.photoUpdated = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.photoCreated = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    PhotoCardComponent.prototype.openDeleteDialog = function () {
        var _this = this;
        var dialogRef = this.dialog.open(__WEBPACK_IMPORTED_MODULE_3__delete_photo_delete_photo_component__["a" /* DeletePhotoComponent */]);
        dialogRef.componentInstance.photo = this.photo;
        dialogRef.afterClosed()
            .subscribe(function (deletedPhoto) {
            if (deletedPhoto) {
                _this.photoDeleted.emit(deletedPhoto);
            }
        });
    };
    PhotoCardComponent.prototype.getCategoryShort = function (category) {
        var aliasObj = {
            bw: 'BW',
            colour: 'CLR',
            mobile: 'MB'
        };
        return aliasObj[category];
    };
    PhotoCardComponent.prototype.editPhoto = function () {
        var _this = this;
        var dialogRef = this.dialog.open(__WEBPACK_IMPORTED_MODULE_4__upload_photo_upload_photo_component__["a" /* UploadPhotoComponent */]);
        dialogRef.componentInstance.existingPhoto = this.photo;
        dialogRef
            .afterClosed()
            .subscribe(function (updatedPhoto) {
            if (updatedPhoto) {
                _this.photoUpdated.emit(updatedPhoto);
            }
        });
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__shared_models_photo_model__["a" /* Photo */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__shared_models_photo_model__["a" /* Photo */]) === "function" && _a || Object)
    ], PhotoCardComponent.prototype, "photo", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], PhotoCardComponent.prototype, "photoDeleted", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], PhotoCardComponent.prototype, "photoUpdated", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], PhotoCardComponent.prototype, "photoCreated", void 0);
    PhotoCardComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-photo-card',
            template: __webpack_require__(224),
            styles: [__webpack_require__(213)]
        }),
        __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_material__["d" /* MdDialog */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_material__["d" /* MdDialog */]) === "function" && _b || Object])
    ], PhotoCardComponent);
    return PhotoCardComponent;
    var _a, _b;
}());

//# sourceMappingURL=photo-card.component.js.map

/***/ }),
/* 137 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(6);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = (function () {
    function AppComponent(router) {
        this.router = router;
    }
    AppComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(227),
            styles: [__webpack_require__(203)]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["Router"]) === "function" && _a || Object])
    ], AppComponent);
    return AppComponent;
    var _a;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),
/* 138 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_router__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_routing__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__public_public_module__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__admin_admin_module__ = __webpack_require__(134);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_7__public_public_module__["a" /* PublicModule */],
                __WEBPACK_IMPORTED_MODULE_8__admin_admin_module__["a" /* AdminModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_router__["RouterModule"].forRoot(__WEBPACK_IMPORTED_MODULE_6__app_routing__["a" /* routes */], { useHash: true })
            ],
            providers: [],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),
/* 139 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return routes; });
var routes = [
    {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
    }
];
//# sourceMappingURL=app.routing.js.map

/***/ }),
/* 140 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_models_photo_model__ = __webpack_require__(29);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PhotoPreviewComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PhotoPreviewComponent = (function () {
    function PhotoPreviewComponent() {
    }
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__shared_models_photo_model__["a" /* Photo */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__shared_models_photo_model__["a" /* Photo */]) === "function" && _a || Object)
    ], PhotoPreviewComponent.prototype, "photo", void 0);
    PhotoPreviewComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-photo-preview',
            template: __webpack_require__(230),
            styles: [__webpack_require__(206)]
        })
    ], PhotoPreviewComponent);
    return PhotoPreviewComponent;
    var _a;
}());

//# sourceMappingURL=photo-preview.component.js.map

/***/ }),
/* 141 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__public_routing__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_greetings_greetings_component__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_category_category_component__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_category_photo_preview_photo_preview_component__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components_category_photo_detailed_photo_detailed_component__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__public_component__ = __webpack_require__(97);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__services_public_photos_service__ = __webpack_require__(52);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PublicModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var PublicModule = (function () {
    function PublicModule() {
    }
    PublicModule = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_10__public_component__["a" /* PublicComponent */],
                __WEBPACK_IMPORTED_MODULE_6__components_greetings_greetings_component__["a" /* GreetingsComponent */],
                __WEBPACK_IMPORTED_MODULE_7__components_category_category_component__["a" /* CategoryComponent */],
                __WEBPACK_IMPORTED_MODULE_8__components_category_photo_preview_photo_preview_component__["a" /* PhotoPreviewComponent */],
                __WEBPACK_IMPORTED_MODULE_9__components_category_photo_detailed_photo_detailed_component__["a" /* PhotoDetailedComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_router__["RouterModule"].forChild(__WEBPACK_IMPORTED_MODULE_5__public_routing__["a" /* routes */])
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_11__services_public_photos_service__["a" /* PublicPhotosService */]
            ],
            bootstrap: [
                __WEBPACK_IMPORTED_MODULE_6__components_greetings_greetings_component__["a" /* GreetingsComponent */]
            ]
        })
    ], PublicModule);
    return PublicModule;
}());

//# sourceMappingURL=public.module.js.map

/***/ }),
/* 142 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_greetings_greetings_component__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_category_category_component__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_category_photo_detailed_photo_detailed_component__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__public_component__ = __webpack_require__(97);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return routes; });




var routes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_3__public_component__["a" /* PublicComponent */],
        children: [
            {
                path: '',
                redirectTo: 'home',
                pathMatch: 'full'
            },
            {
                path: 'home',
                component: __WEBPACK_IMPORTED_MODULE_0__components_greetings_greetings_component__["a" /* GreetingsComponent */]
            },
            {
                path: 'category/:category',
                component: __WEBPACK_IMPORTED_MODULE_1__components_category_category_component__["a" /* CategoryComponent */],
            },
            {
                path: 'detailed/:id',
                component: __WEBPACK_IMPORTED_MODULE_2__components_category_photo_detailed_photo_detailed_component__["a" /* PhotoDetailedComponent */]
            }
        ]
    }
];
//# sourceMappingURL=public.routing.js.map

/***/ }),
/* 143 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_cdk__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_startWith__ = __webpack_require__(241);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_startWith___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_startWith__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_observable_merge__ = __webpack_require__(238);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_observable_merge___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_observable_merge__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_map__ = __webpack_require__(240);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_map__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TableComponent; });
/* unused harmony export ExampleDatabase */
/* unused harmony export ExampleDataSource */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var TableComponent = (function () {
    function TableComponent() {
        this.displayedColumns = ['userId', 'userName', 'progress', 'color'];
        this.exampleDatabase = new ExampleDatabase();
    }
    TableComponent.prototype.ngOnInit = function () {
        this.dataSource = new ExampleDataSource(this.exampleDatabase, this.paginator);
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_2__angular_material__["b" /* MdPaginator */]),
        __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_material__["b" /* MdPaginator */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_material__["b" /* MdPaginator */]) === "function" && _a || Object)
    ], TableComponent.prototype, "paginator", void 0);
    TableComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'shared-data-table',
            styles: [__webpack_require__(216)],
            template: __webpack_require__(233),
        })
    ], TableComponent);
    return TableComponent;
    var _a;
}());

/** Constants used to fill up our data base. */
var COLORS = ['maroon', 'red', 'orange', 'yellow', 'olive', 'green', 'purple',
    'fuchsia', 'lime', 'teal', 'aqua', 'blue', 'navy', 'black', 'gray'];
var NAMES = ['Maia', 'Asher', 'Olivia', 'Atticus', 'Amelia', 'Jack',
    'Charlotte', 'Theodore', 'Isla', 'Oliver', 'Isabella', 'Jasper',
    'Cora', 'Levi', 'Violet', 'Arthur', 'Mia', 'Thomas', 'Elizabeth'];
/** An example database that the data source uses to retrieve data for the table. */
var ExampleDatabase = (function () {
    function ExampleDatabase() {
        /** Stream that emits whenever the data has been modified. */
        this.dataChange = new __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__["BehaviorSubject"]([]);
        // Fill up the database with 100 users.
        for (var i = 0; i < 100; i++) {
            this.addUser();
        }
    }
    Object.defineProperty(ExampleDatabase.prototype, "data", {
        get: function () {
            return this.dataChange.value;
        },
        enumerable: true,
        configurable: true
    });
    /** Adds a new user to the database. */
    ExampleDatabase.prototype.addUser = function () {
        var copiedData = this.data.slice();
        copiedData.push(this.createNewUser());
        this.dataChange.next(copiedData);
    };
    /** Builds and returns a new User. */
    ExampleDatabase.prototype.createNewUser = function () {
        var name = NAMES[Math.round(Math.random() * (NAMES.length - 1))] + ' ' +
            NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) + '.';
        return {
            id: (this.data.length + 1).toString(),
            name: name,
            progress: Math.round(Math.random() * 100).toString(),
            color: COLORS[Math.round(Math.random() * (COLORS.length - 1))]
        };
    };
    return ExampleDatabase;
}());

/**
 * Data source to provide what data should be rendered in the table. Note that the data source
 * can retrieve its data in any way. In this case, the data source is provided a reference
 * to a common data base, ExampleDatabase. It is not the data source's responsibility to manage
 * the underlying data. Instead, it only needs to take the data and send the table exactly what
 * should be rendered.
 */
var ExampleDataSource = (function (_super) {
    __extends(ExampleDataSource, _super);
    function ExampleDataSource(_exampleDatabase, _paginator) {
        var _this = _super.call(this) || this;
        _this._exampleDatabase = _exampleDatabase;
        _this._paginator = _paginator;
        return _this;
    }
    /** Connect function called by the table to retrieve one stream containing the data to render. */
    ExampleDataSource.prototype.connect = function () {
        var _this = this;
        var displayDataChanges = [
            this._exampleDatabase.dataChange,
            this._paginator.page,
        ];
        return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].merge.apply(__WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"], displayDataChanges).map(function () {
            var data = _this._exampleDatabase.data.slice();
            // Grab the page's slice of data.
            var startIndex = _this._paginator.pageIndex * _this._paginator.pageSize;
            return data.splice(startIndex, _this._paginator.pageSize);
        });
    };
    ExampleDataSource.prototype.disconnect = function () {
    };
    return ExampleDataSource;
}(__WEBPACK_IMPORTED_MODULE_1__angular_cdk__["b" /* DataSource */]));

//# sourceMappingURL=table.component.js.map

/***/ }),
/* 144 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser_animations__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_table_table_component__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_material__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_cdk__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__requesters_http_service_requester__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_angular4_notify__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_angular4_notify___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_angular4_notify__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SharedModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var SharedModule = (function () {
    function SharedModule() {
    }
    SharedModule = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__components_table_table_component__["a" /* TableComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_9_angular4_notify__["NotificationsModule"],
                __WEBPACK_IMPORTED_MODULE_6__angular_material__["a" /* MaterialModule */],
                __WEBPACK_IMPORTED_MODULE_7__angular_cdk__["a" /* CdkTableModule */]
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_5__components_table_table_component__["a" /* TableComponent */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_8__requesters_http_service_requester__["a" /* HttpServiceRequester */],
                __WEBPACK_IMPORTED_MODULE_9_angular4_notify__["NotificationsService"]
            ],
            bootstrap: []
        })
    ], SharedModule);
    return SharedModule;
}());

//# sourceMappingURL=shared.module.js.map

/***/ }),
/* 145 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaseError; });
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var BaseError = (function (_super) {
    __extends(BaseError, _super);
    function BaseError(message, codeNum) {
        var _this = _super.call(this, message) || this;
        _this.code = 0;
        _this.code = codeNum;
        return _this;
    }
    return BaseError;
}(Error));

//# sourceMappingURL=base.error.js.map

/***/ }),
/* 146 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__base_error__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__errors_numbers__ = __webpack_require__(53);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UnauthorizedRequestError; });
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();


var UnauthorizedRequestError = (function (_super) {
    __extends(UnauthorizedRequestError, _super);
    function UnauthorizedRequestError(message) {
        return _super.call(this, message, __WEBPACK_IMPORTED_MODULE_1__errors_numbers__["a" /* default */].UNAUTHORIZED_REQUEST_ERROR) || this;
    }
    return UnauthorizedRequestError;
}(__WEBPACK_IMPORTED_MODULE_0__base_error__["a" /* BaseError */]));

//# sourceMappingURL=unauthorized-request.error.js.map

/***/ }),
/* 147 */,
/* 148 */,
/* 149 */,
/* 150 */,
/* 151 */,
/* 152 */,
/* 153 */,
/* 154 */,
/* 155 */,
/* 156 */,
/* 157 */,
/* 158 */,
/* 159 */,
/* 160 */,
/* 161 */,
/* 162 */,
/* 163 */,
/* 164 */,
/* 165 */,
/* 166 */,
/* 167 */,
/* 168 */,
/* 169 */,
/* 170 */,
/* 171 */,
/* 172 */,
/* 173 */,
/* 174 */,
/* 175 */,
/* 176 */,
/* 177 */,
/* 178 */,
/* 179 */,
/* 180 */,
/* 181 */,
/* 182 */,
/* 183 */,
/* 184 */,
/* 185 */,
/* 186 */,
/* 187 */,
/* 188 */,
/* 189 */,
/* 190 */,
/* 191 */,
/* 192 */,
/* 193 */,
/* 194 */,
/* 195 */,
/* 196 */,
/* 197 */,
/* 198 */,
/* 199 */,
/* 200 */,
/* 201 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)(false);
// imports


// module
exports.push([module.i, ".admin-container {\n    width: 100%;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),
/* 202 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)(false);
// imports


// module
exports.push([module.i, ".auth-container {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    min-height: 80vh;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),
/* 203 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),
/* 204 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)(false);
// imports


// module
exports.push([module.i, ".category-container {\n    /*width: 100%;*/\n\n    /*column-count: 2;*/\n    /*-moz-column-count: 2;*/\n    /*-webkit-column-count: 2;*/\n    /*column-gap: 2em;*/\n    -webkit-column-width: 400px;\n            column-width: 400px;\n    /*column-fill: balance;*/\n\n    text-align: center;\n    /*margin: 20px;*/\n}\n\n@media (max-width: 700px) {\n    .category-container {\n        -webkit-column-count: 2;\n        column-count: 2;\n    }\n}\n\n@media (min-width: 700px) and (max-width: 1100px) {\n    .category-container {\n        -webkit-column-count: 3;\n        column-count: 3;\n    }\n}\n\n@media (min-width: 1100px) and (max-width: 1400px) {\n    .category-container {\n        -webkit-column-count: 3;\n        column-count: 3;\n    }\n}\n\n@media (min-width: 1400px) {\n    .category-container {\n        -webkit-column-count: 4;\n        column-count: 4;\n    }\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),
/* 205 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)(false);
// imports


// module
exports.push([module.i, ".photo-detailed-container {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n}\n\n.photo {\n    width: auto;\n    max-height: 70vh;\n    -ms-flex-item-align: center;\n        -ms-grid-row-align: center;\n        align-self: center;\n}\n\n.details {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: horizontal;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: row;\n            flex-direction: row;\n    -ms-flex-wrap: wrap;\n        flex-wrap: wrap;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    font-size: 24px;\n    margin-top: 16px;\n    margin-left: 20%;\n}\n\n.share img {\n    height: 24px;\n}\n\n.navigation  i {\n    font-size: 34px;\n    cursor: pointer;\n}\n\n.name, .share, .navigation {\n    width: 33%;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),
/* 206 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)(false);
// imports


// module
exports.push([module.i, ".photo-preview-container img {\n    width: 400px;\n}\n\n.photo-preview-container img:hover {\n    opacity: 0.5;\n}\n\n.photo-preview-container {\n    margin-bottom: 20px;\n}\n\n.photo-preview-container img {\n    cursor: pointer;\n}\n\n.open-full {\n    font-size: 40px;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),
/* 207 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)(false);
// imports


// module
exports.push([module.i, "#greetings-container {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    padding-left: 40%;\n    padding-top: 5%;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),
/* 208 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)(false);
// imports


// module
exports.push([module.i, "#app-container {\n    min-height: 100vh;\n    background-size: cover;\n}\n\n#app-container.main-page {\n    background-image: url(/assets/images/bg.jpg);\n}\n\n.top-menu {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    padding: 30px;\n    -webkit-box-orient: horizontal;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: row;\n            flex-direction: row;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n\n.navigation-menu {\n    padding-left: 20%;\n    /*margin-right: 20%;*/\n    font-size: 28px;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    width: 40%;\n    -ms-flex-pack: distribute;\n        justify-content: space-around;\n}\n\n.navigation-menu > a {\n    color: #000;\n    padding: 10px;\n    margin-right: 50px;\n    text-decoration: none;\n}\n\n.navigation-menu > a:hover, .navigation-menu > a.active  {\n    background-color: transparent;\n    box-shadow: 0 0 1pt 1pt #000;\n    border-radius: 5px;\n    padding-top: 10px;\n    padding-bottom: 10px;\n    -kthtml-transition: all 0.5s linear;\n    transition: all 0.5s linear;\n}\n\n.follow-me-container {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n\n.follow-me-container .row {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: justify;\n        -ms-flex-pack: justify;\n            justify-content: space-between;\n}\n\n.follow-me-container img {\n    width: 30px;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),
/* 209 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)(false);
// imports


// module
exports.push([module.i, ".dashboard-container {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: start;\n      -ms-flex-pack: start;\n          justify-content: flex-start; }\n\n.left-menu {\n  min-width: 20%;\n  height: 100vh;\n  background: white;\n  background: -webkit-gradient(left top, right top, color-stop(0%, white), color-stop(47%, #f6f6f6), color-stop(100%, #ededed));\n  background: linear-gradient(to right, white 0%, #f6f6f6 47%, #ededed 100%);\n  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#ededed', GradientType=1 ); }\n\n.mat-button {\n  width: 100%; }\n\na.active {\n  background-color: #ccd3ff; }\n\n.content {\n  width: 100%;\n  padding: 24px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),
/* 210 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),
/* 211 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),
/* 212 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)(false);
// imports


// module
exports.push([module.i, ".btns-container {\n  margin-bottom: 16px; }\n  .btns-container a {\n    margin-right: 6px; }\n\n.photos-container {\n  -ms-flex-wrap: wrap;\n      flex-wrap: wrap; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),
/* 213 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)(false);
// imports


// module
exports.push([module.i, ".photo-card {\n  width: 250px;\n  margin-bottom: 24px;\n  margin-right: 24px; }\n\nmd-card-header {\n  margin-bottom: 5px; }\n\n.circle {\n  width: 40px;\n  height: 40px;\n  border-radius: 20px;\n  color: #fff;\n  line-height: 40px;\n  text-align: center;\n  background: #000; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),
/* 214 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)(false);
// imports


// module
exports.push([module.i, ".upload-photo-container {\n  width: 400px; }\n\ntextarea {\n  resize: vertical; }\n\n.btns-row {\n  margin-top: 24px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),
/* 215 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),
/* 216 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)(false);
// imports


// module
exports.push([module.i, "/* Structure */\n.example-container {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  max-height: 500px;\n  min-width: 300px; }\n\n.example-header {\n  min-height: 64px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  padding-left: 24px;\n  font-size: 20px; }\n\n.mat-table {\n  overflow: auto; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),
/* 217 */,
/* 218 */
/***/ (function(module, exports) {

module.exports = "<md-toolbar color=\"primary\">\n    <span>Admin</span>\n    <span class=\"space-to-right\"></span>\n\n    <section *ngIf=\"authService.authUser\">\n        <md-menu #authedMenu=\"mdMenu\">\n            <button md-menu-item> Profile</button>\n            <button md-menu-item (click)=\"doLogout()\"> Logout</button>\n        </md-menu>\n\n        <button md-button [mdMenuTriggerFor]=\"authedMenu\">\n            {{authService.authUser.email}}\n            <md-icon>arrow_drop_down</md-icon>\n        </button>\n    </section>\n\n    <section *ngIf=\"!authService.authUser\">\n        <button md-button [routerLink]=\"['/admin', 'auth', 'login']\">Login</button>\n    </section>\n\n</md-toolbar>\n\n<angular4-notify-notifications-container></angular4-notify-notifications-container>\n\n<div class=\"admin-container\">\n    <router-outlet></router-outlet>\n</div>\n"

/***/ }),
/* 219 */
/***/ (function(module, exports) {

module.exports = "<div class=\"auth-container\">\n    <md-card>\n        <md-card-header>\n            <md-card-title>Login</md-card-title>\n        </md-card-header>\n\n        <md-card-content>\n            <md-input-container>\n                <input mdInput placeholder=\"Email\" [(ngModel)]=\"model.email\">\n            </md-input-container>\n            <md-input-container>\n                <input mdInput placeholder=\"Password\" type=\"password\" [(ngModel)]=\"model.password\">\n            </md-input-container>\n        </md-card-content>\n\n        <md-card-actions>\n            <button md-raised-button color=\"primary\" (click)=\"login()\">Login</button>\n        </md-card-actions>\n    </md-card>\n</div>\n"

/***/ }),
/* 220 */
/***/ (function(module, exports) {

module.exports = "<div class=\"dashboard-container\">\n    <div class=\"left-menu\">\n        <md-list>\n            <md-list-item>\n                <a md-button [routerLink]=\"['general']\" routerLinkActive=\"active\">General</a>\n            </md-list-item>\n            <md-list-item>\n                <a md-button [routerLink]=\"['photos']\" routerLinkActive=\"active\">Photos</a>\n            </md-list-item>\n            <md-list-item>\n                <a md-button [routerLink]=\"['settings']\">Settings</a>\n            </md-list-item>\n        </md-list>\n    </div>\n\n    <div class=\"content\">\n        <router-outlet></router-outlet>\n    </div>\n</div>"

/***/ }),
/* 221 */
/***/ (function(module, exports) {

module.exports = "general component"

/***/ }),
/* 222 */
/***/ (function(module, exports) {

module.exports = "<div class=\"delete-photo-container\">\n    <p>Delete <u>{{photo.name}}</u> photo?</p>\n\n    <div class=\"row\">\n        <a md-raised-button color=\"warn\" (click)=\"deletePhoto()\">Delete</a>\n        <a md-raised-button color=\"default\" (click)=\"cancel()\">Cancel</a>\n    </div>\n</div>"

/***/ }),
/* 223 */
/***/ (function(module, exports) {

module.exports = "<h3>Uploaded photos</h3>\n\n<div class=\"flex-container btns-container\">\n    <md-select placeholder=\"Filter by category\" [(ngModel)]=\"filter.category\" [multiple]=\"true\" (change)=\"filterPhotosList($event)\">\n        <md-option *ngFor=\"let category of categoriesList\" [value]=\"category\">\n            {{category}}\n        </md-option>\n    </md-select>\n</div>\n\n<div class=\"flex-container btns-container\">\n    <a md-raised-button color=\"accent\" (click)=\"openUploadDialog()\">Upload new</a>\n    <!--<a md-raised-button color=\"warn\">Delete</a>-->\n</div>\n\n<div class=\"flex-container photos-container\">\n    <app-photo-card *ngFor=\"let photo of filteredPhotos\" [photo]=\"photo\"\n                    (photoDeleted)=\"onPhotoDeleted($event)\" (photoUpdated)=\"onPhotoUpdated($event)\"></app-photo-card>\n    <h4 *ngIf=\"filteredPhotos?.length === 0\">No photos.</h4>\n</div>\n"

/***/ }),
/* 224 */
/***/ (function(module, exports) {

module.exports = "<md-card class=\"photo-card\">\n    <md-card-header>\n        <div md-card-avatar>\n            <div class=\"circle\">\n                {{getCategoryShort(photo.category)}}\n            </div>\n        </div>\n        <md-card-title>{{photo.name}}</md-card-title>\n        <md-card-subtitle>{{photo.description}}</md-card-subtitle>\n    </md-card-header>\n    <img md-card-image src=\"/uploads/small-{{photo.filename}}\">\n    <md-card-actions>\n        <button md-button (click)=\"editPhoto()\">EDIT</button>\n        <button md-button (click)=\"openDeleteDialog()\">DELETE</button>\n    </md-card-actions>\n</md-card>\n"

/***/ }),
/* 225 */
/***/ (function(module, exports) {

module.exports = "<div class=\"upload-photo-container\">\n    <h3>Upload new photo</h3>\n\n    <div class=\"row\">\n        <md-select placeholder=\"Category\" class=\"full-width\" [(ngModel)]=\"model.category\">\n            <md-option *ngFor=\"let category of categoriesList\" [value]=\"category\">\n                {{ category }}\n            </md-option>\n        </md-select>\n    </div>\n\n    <div class=\"row\">\n        <md-input-container class=\"full-width\">\n            <input mdInput placeholder=\"Name\" [(ngModel)]=\"model.name\">\n        </md-input-container>\n    </div>\n\n    <div class=\"row\">\n        <md-input-container class=\"full-width\">\n            <textarea mdInput name=\"description\" placeholder=\"Description\" [(ngModel)]=\"model.description\"></textarea>\n        </md-input-container>\n    </div>\n\n    <div class=\"row\">\n        <input type=\"file\" name=\"image_file\" ng4-file-submit>\n    </div>\n\n    <div class=\"row btns-row\">\n        <button md-raised-button color=\"primary\" (click)=\"uploadPhoto()\">Submit</button>\n    </div>\n</div>\n"

/***/ }),
/* 226 */
/***/ (function(module, exports) {

module.exports = "settings component"

/***/ }),
/* 227 */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),
/* 228 */
/***/ (function(module, exports) {

module.exports = "<div class=\"category-container\">\n    <app-photo-preview *ngFor=\"let photo of photos\" [photo]=\"photo\"></app-photo-preview>\n\n    <h2 *ngIf=\"photos.length === 0\">No photos.</h2>\n</div>"

/***/ }),
/* 229 */
/***/ (function(module, exports) {

module.exports = "<div class=\"photo-detailed-container\" *ngIf=\"photo\">\n    <img class=\"photo\" src=\"/uploads/large-{{photo.filename}}\">\n\n    <div class=\"details\">\n        <div class=\"name\">\n            {{photo.name}}\n        </div>\n        <div class=\"share\">\n            <a href=\"\"><img src=\"/assets/images/social/share.png\"></a>\n            <a href=\"\"><img src=\"/assets/images/social/facebook.png\"></a>\n            <a href=\"\"><img src=\"/assets/images/social/twitter.png\"></a>\n        </div>\n        <div class=\"navigation\">\n            <i class=\"material-icons\" *ngIf=\"showPhotoObj.next_id\" [routerLink]=\"['/detailed', showPhotoObj.next_id]\">navigate_before</i>\n            <i class=\"material-icons\" [routerLink]=\"['/category', photo.category]\">view_module</i>\n            <i class=\"material-icons\" *ngIf=\"showPhotoObj.prev_id\" [routerLink]=\"['/detailed', showPhotoObj.prev_id]\">navigate_next</i>\n        </div>\n\n    </div>\n</div>"

/***/ }),
/* 230 */
/***/ (function(module, exports) {

module.exports = "<div class=\"photo-preview-container\" [routerLink]=\"['/detailed', photo.id]\">\n    <img src=\"/uploads/small-{{photo.filename}}\">\n    <!--<i class=\"material-icons open-full\">open_with</i> TODO show as overlay-->\n</div>"

/***/ }),
/* 231 */
/***/ (function(module, exports) {

module.exports = "<div id=\"greetings-container\">\n    <h1>Holla! I'm Ann.</h1>\n    <h1>Photographer and web designer</h1>\n\n    <h4>Currently living in Kharkiv, UA</h4>\n</div>"

/***/ }),
/* 232 */
/***/ (function(module, exports) {

module.exports = "<div id=\"app-container\" [class.main-page]=\"router.isActive('/home')\">\n    <div class=\"top-menu\">\n        <a [routerLink]=\"['/']\">\n            <img src=\"/assets/images/logo1.png\">\n        </a>\n\n        <div class=\"navigation-menu\">\n            <a [routerLink]=\"['/category', 'bw']\" routerLinkActive=\"active\">b&w</a>\n            <a [routerLink]=\"['/category', 'colour']\" routerLinkActive=\"active\">colour</a>\n            <a [routerLink]=\"['/category', 'mobile']\" routerLinkActive=\"active\">mobile</a>\n            <a [routerLink]=\"['/category', 'info']\" routerLinkActive=\"active\">info</a>\n        </div>\n\n        <div class=\"space-to-right\"></div>\n\n        <div class=\"follow-me-container\">\n            <div class=\"row\">FOLLOW ME</div>\n            <div class=\"row\">\n                <a href=\"\"><img src=\"/assets/images/social/facebook.png\"></a>\n                <a href=\"\"><img src=\"/assets/images/social/instagram.png\"></a>\n            </div>\n        </div>\n    </div>\n\n    <router-outlet></router-outlet>\n</div>"

/***/ }),
/* 233 */
/***/ (function(module, exports) {

module.exports = "<div class=\"example-container mat-elevation-z8\">\n\n    <md-table #table [dataSource]=\"dataSource\">\n\n        <!--- Note that these columns can be defined in any order.\n              The actual rendered columns are set as a property on the row definition\" -->\n\n        <!-- ID Column -->\n        <ng-container cdkColumnDef=\"userId\">\n            <md-header-cell *cdkHeaderCellDef> ID</md-header-cell>\n            <md-cell *cdkCellDef=\"let row\"> {{row.id}}</md-cell>\n        </ng-container>\n\n        <!-- Progress Column -->\n        <ng-container cdkColumnDef=\"progress\">\n            <md-header-cell *cdkHeaderCellDef> Progress</md-header-cell>\n            <md-cell *cdkCellDef=\"let row\"> {{row.progress}}%</md-cell>\n        </ng-container>\n\n        <!-- Name Column -->\n        <ng-container cdkColumnDef=\"userName\">\n            <md-header-cell *cdkHeaderCellDef> Name</md-header-cell>\n            <md-cell *cdkCellDef=\"let row\"> {{row.name}}</md-cell>\n        </ng-container>\n\n        <!-- Color Column -->\n        <ng-container cdkColumnDef=\"color\">\n            <md-header-cell *cdkHeaderCellDef> Color</md-header-cell>\n            <md-cell *cdkCellDef=\"let row\" [style.color]=\"row.color\"> {{row.color}}</md-cell>\n        </ng-container>\n\n        <md-header-row *cdkHeaderRowDef=\"displayedColumns\"></md-header-row>\n        <md-row *cdkRowDef=\"let row; columns: displayedColumns;\"></md-row>\n    </md-table>\n\n    <md-paginator #paginator\n                  [length]=\"exampleDatabase.data.length\"\n                  [pageIndex]=\"0\"\n                  [pageSize]=\"25\"\n                  [pageSizeOptions]=\"[5, 10, 25, 100]\">\n    </md-paginator>\n</div>"

/***/ }),
/* 234 */,
/* 235 */,
/* 236 */,
/* 237 */,
/* 238 */,
/* 239 */,
/* 240 */,
/* 241 */,
/* 242 */,
/* 243 */,
/* 244 */,
/* 245 */,
/* 246 */,
/* 247 */,
/* 248 */,
/* 249 */,
/* 250 */,
/* 251 */,
/* 252 */,
/* 253 */,
/* 254 */,
/* 255 */,
/* 256 */,
/* 257 */,
/* 258 */,
/* 259 */,
/* 260 */,
/* 261 */,
/* 262 */,
/* 263 */,
/* 264 */,
/* 265 */,
/* 266 */,
/* 267 */,
/* 268 */,
/* 269 */,
/* 270 */,
/* 271 */,
/* 272 */,
/* 273 */,
/* 274 */,
/* 275 */,
/* 276 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(128);


/***/ })
]),[276]);
//# sourceMappingURL=main.bundle.js.map