'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        queryInterface.createTable(
            'photos',
            {
                id: {
                    type: Sequelize.INTEGER.UNSIGNED,
                    primaryKey: true,
                    autoIncrement: true
                },

                created_at: {
                    type: Sequelize.DATE
                },
                updated_at: {
                    type: Sequelize.DATE
                },
                deleted_at: {
                    type: Sequelize.DATE
                },

                user_id: {
                    type: Sequelize.INTEGER.UNSIGNED,
                    references: {
                        model: 'users',
                        key: 'id'
                    },
                    onDelete: 'cascade'
                },

                name: {
                    type: Sequelize.STRING,
                    allowNull: false
                },
                description: {
                    type: Sequelize.STRING,
                },
                filename: {
                    type: Sequelize.STRING,
                    allowNull: false
                }
            }
        );
    },

    down: function (queryInterface, Sequelize) {
        queryInterface.dropTable('photos');
    }
};
