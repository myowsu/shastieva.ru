'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
      queryInterface.addColumn(
          'photos',
          'category',
          {
              type: Sequelize.STRING,
              allowNull: false
          }
      )
  },

  down: function (queryInterface, Sequelize) {
      queryInterface.removeColumn('photos', 'category');
  }
};
