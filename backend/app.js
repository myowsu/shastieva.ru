let express = require('express');
let bodyParser = require('body-parser');
let config = require('config');
let cookieParser = require('cookie-parser');
const fileUpload = require('express-fileupload');

const morgan = require('morgan');

let app = express();

app.use(cookieParser());

app.use(bodyParser.urlencoded({
    extended: true,
    limit: '50mb'
}));
app.use(bodyParser.json({
    extended: true,
    parameterLimit: 10000,
    limit: '50mb'
}));

app.use(fileUpload());
app.use(express.static('dist'));
app.use('/uploads', express.static('uploads'));

if (process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'));
}

require('./db/sequelize-db-connect')
    .authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
    });

app.use((req, res, next) => {
    const allowedOrigins = [
        'http://127.0.0.1:4200',
        'http://localhost:4200',
    ];

    let origin = req.headers.origin;

    if (allowedOrigins.indexOf(origin) !== -1) {
        res.setHeader('Access-Control-Allow-Origin', origin);
    }

    res.header('Access-Control-Allow-Credentials', true);
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    return next();
});

app.use('/auth', require('./routes/auth.routes')());
app.use('/admin', require('./routes/admin/index.routes'));
app.use('/public', require('./routes/public/index.routes'));

require('./errors.handler')(app);

app.listen(config.SERVER_PORT, () => {
    console.log('Trainer backend started. Port: ', config.SERVER_PORT);
});