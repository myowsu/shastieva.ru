import DateTimeFormat = Intl.DateTimeFormat;

export class Photo {
    id?: number;
    name: string;
    description?: string;
    category: string;
    filename?: string;
    created_at?: DateTimeFormat;
}

export class ShowPhotoObject {
    photo: Photo;
    prev_id: number;
    next_id: number
}

export const CATEGORIES_LIST = [
    'bw',
    'colour',
    'mobile'
];