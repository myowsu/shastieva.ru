import {Http, Headers, RequestOptions, URLSearchParams} from '@angular/http';
import {Injectable} from '@angular/core/';
import 'rxjs/add/operator/toPromise';
import {Router} from '@angular/router';
import {environment} from "../../../environments/environment";
import {UnauthorizedRequestError} from "../../../errors/unauthorized-request.error";

import {NotificationsService} from "angular4-notify";

@Injectable()
export class HttpServiceRequester {
    constructor(protected http: Http,
                protected router: Router,
                protected notificationsService: NotificationsService
    ) {

    }

    protected baseUrl = environment.baseUrl;

    makeRequest(action: string, method: string, data: {} = {}) {
        let params, body;

        const headers = new Headers({
            'Content-Type': 'application/json',
        });

        console.log('make http call', action, data);

        if (method === 'GET' || method === 'DELETE') {
            params = new URLSearchParams();
            for (const prop in data) {
                if (data.hasOwnProperty(prop)) {
                    const value = data[prop];

                    if (Array.isArray(value)) {
                        value.forEach(function (val) {
                            params.append(prop + '[]', val);
                        });
                    } else {
                        params.set(prop, data[prop]);
                    }
                }
            }
        } else if (['POST', 'PUT', 'PATCH'].indexOf(method) !== -1) {
            body = JSON.stringify(data);
        }

        const requestOptions = new RequestOptions({
            headers: headers,
            method: method,
            search: params ? params : null,
            body: body ? body : null,
            withCredentials: true
        });

        return this.http.request(this.baseUrl + action, requestOptions)
            .toPromise()
            .then(response => {

                // todo check if 202 code is really needed
                if (response.status === 202) {
                    return this.handleError(response);
                }

                const resObj = response.json();

                return resObj.result;
            })
            .catch((response: any) => {
                return this.handleError(response);
            });
    }

    submitFormData(action: string, method: string, data: FormData) {
        return this.http.request(
            this.baseUrl + action,
            {
                method: method,
                withCredentials: true,
                body: data,
            })
            .toPromise()
            .then(response => {

                // todo check if 202 code is really needed
                if (response.status === 202) {
                    return this.handleError(response);
                }

                const resObj = response.json();

                return resObj.result;
            })
            .catch((response: any) => {
                return this.handleError(response);
            });
    }

    protected handleError(response: any) {
        let message = response.statusText;
        let errorCode;

        if (response['_body']) {
            try {
                const jsonBody = JSON.parse(response['_body']);

                if (jsonBody.message) {
                    message = jsonBody.message;
                }

                if (jsonBody.result) {
                    errorCode = jsonBody.code;
                }

            } catch (err) {
                message = 'Error while parsing response server. Service unavailable.';
            }
        }

        if (response.status === 403) {
            throw new UnauthorizedRequestError('You are not authorized to perform this request');
        }

        this.notificationsService.addError(message);

        throw new Error(message);
    }
}
