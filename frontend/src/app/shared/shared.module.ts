import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {TableComponent} from "./components/table/table.component";
import {MaterialModule} from "@angular/material";
import {CdkTableModule} from "@angular/cdk";
import {HttpServiceRequester} from "./requesters/http.service.requester";

import {NotificationsModule, NotificationsService} from "angular4-notify";

@NgModule({
    declarations: [
        TableComponent
    ],
    imports: [
        BrowserAnimationsModule,
        BrowserModule,
        FormsModule,
        HttpModule,
        NotificationsModule,
        MaterialModule,
        CdkTableModule
    ],
    exports: [
        TableComponent
    ],
    providers: [
        HttpServiceRequester,
        NotificationsService
    ],
    bootstrap: []
})
export class SharedModule {
}
