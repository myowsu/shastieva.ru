import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {RouterModule} from "@angular/router";
import {routes} from "./public.routing";
import {GreetingsComponent} from "./components/greetings/greetings.component";
import {CategoryComponent} from './components/category/category.component';
import {PhotoPreviewComponent} from './components/category/photo-preview/photo-preview.component';
import {PhotoDetailedComponent} from './components/category/photo-detailed/photo-detailed.component';
import {PublicComponent} from "./public.component";
import {PublicPhotosService} from "./services/public.photos.service";

@NgModule({
    declarations: [
        PublicComponent,
        GreetingsComponent,
        CategoryComponent,
        PhotoPreviewComponent,
        PhotoDetailedComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        RouterModule.forChild(routes)
    ],
    providers: [
        PublicPhotosService
    ],
    bootstrap: [
        GreetingsComponent
    ]
})
export class PublicModule {
}
