import {Injectable} from '@angular/core';
import {HttpServiceRequester} from '../../shared/requesters/http.service.requester';
import {Photo, ShowPhotoObject} from "../../shared/models/photo.model";

@Injectable()
export class PublicPhotosService {
    constructor(protected requester: HttpServiceRequester) {

    }

    list(category?: string): Promise<Photo[]> {
        return this.requester.makeRequest(
            '/public/photos',
            'GET', {
                category: category
            }
        );
    }

    show(id: number): Promise<ShowPhotoObject> {
        return this.requester.makeRequest(
            '/public/photos/' + id,
            'GET'
        );
    }
}
