import {Routes} from '@angular/router';
import {GreetingsComponent} from './components/greetings/greetings.component';
import {CategoryComponent} from './components/category/category.component';
import {PhotoDetailedComponent} from './components/category/photo-detailed/photo-detailed.component';
import {PublicComponent} from './public.component';

export const routes: Routes = [
    {
        path: '',
        component: PublicComponent,
        children: [
            {
                path: '',
                redirectTo: 'home',
                pathMatch: 'full'
            },
            {
                path: 'home',
                component: GreetingsComponent
            },
            {
                path: 'category/:category',
                component: CategoryComponent,
            },
            {
                path: 'detailed/:id',
                component: PhotoDetailedComponent
            }
        ]
    }
];