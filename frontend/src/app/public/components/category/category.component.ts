import {Component, OnInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {PublicPhotosService} from "../../services/public.photos.service";
import {Photo} from "../../../shared/models/photo.model";

@Component({
    selector: 'app-category',
    templateUrl: './category.component.html',
    styleUrls: [
        './category.component.css'
    ]
})
export class CategoryComponent implements OnInit {
    protected category: string;
    protected photos: Photo[] = [];

    constructor(protected activatedRoute: ActivatedRoute,
                protected publicPhotosService: PublicPhotosService) {

    }

    ngOnInit() {
        this.activatedRoute.params.subscribe(
            params => {
                this.category = params['category'];

                return this.getPhotos(this.category);
            }
        );
    }

    getPhotos(category: string) {
        return this.publicPhotosService.list(category)
            .then(photos => {
                this.photos = photos;
            })
    }
}
