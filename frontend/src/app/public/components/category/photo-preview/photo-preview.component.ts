import {Component, Input} from '@angular/core';
import {Photo} from "../../../../shared/models/photo.model";

@Component({
    selector: 'app-photo-preview',
    templateUrl: './photo-preview.component.html',
    styleUrls: [
        './photo-preview.component.css'
    ]
})
export class PhotoPreviewComponent {
    @Input() photo: Photo;
}
