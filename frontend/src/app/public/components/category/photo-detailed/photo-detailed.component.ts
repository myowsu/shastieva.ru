import {Component, OnInit} from "@angular/core";
import {Photo, ShowPhotoObject} from "../../../../shared/models/photo.model";
import {ActivatedRoute} from "@angular/router";
import {PublicPhotosService} from "../../../services/public.photos.service";

@Component({
    selector: 'app-photo-detailed',
    templateUrl: './photo-detailed.component.html',
    styleUrls: [
        './photo-detailed.component.css'
    ]
})
export class PhotoDetailedComponent implements OnInit {
    protected photo: Photo;
    protected showPhotoObj: ShowPhotoObject;

    constructor(protected activatedRoute: ActivatedRoute, protected publicPhotosService: PublicPhotosService) {

    }

    ngOnInit() {
        this.activatedRoute.params.subscribe(
            params => {
                const id = +params['id'];

                return this.getPhoto(id);
            }
        );
    }

    getPhoto(id: number) {
        return this.publicPhotosService.show(id)
            .then(showPhotoObj => {
                this.photo = showPhotoObj.photo;
                this.showPhotoObj = showPhotoObj;
            });
    }
}
