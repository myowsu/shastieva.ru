import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {RouterModule} from "@angular/router";
import {routes} from "./admin.routing";
import {MaterialModule} from '@angular/material';
import {LoginComponent} from './components/auth/login/login.component';
import {AuthService} from './services/auth.service';

import 'hammerjs';
import {AuthResolver} from './services/resolvers/auth.resolver';
import {HttpServiceRequester} from '../shared/requesters/http.service.requester';
import {AdminComponent} from './admin.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {PhotosIndexComponent} from "./components/dashboard/photos/index/index.component";
import {GuestResolver} from "./services/resolvers/guest.resolver";
import {SettingsComponent} from "./components/dashboard/settings/settings.component";
import {SharedModule} from "../shared/shared.module";
import {PhotoCardComponent} from "./components/dashboard/photos/photo-card/photo-card.component";
import {UploadPhotoComponent} from "./components/dashboard/photos/upload-photo/upload-photo.component";
import {PhotosService} from "./services/photos.service";
import {DeletePhotoComponent} from "./components/dashboard/photos/delete-photo/delete-photo.component";
import {DashboardComponent} from "app/admin/components/dashboard/dashboard.component";
import {GeneralComponent} from "./components/dashboard/general/general.component";

import {NotificationsModule, NotificationsService} from "angular4-notify";
import {FileSubmitModule} from "angular4-file-submit";

@NgModule({
    declarations: [
        LoginComponent,
        AdminComponent,

        PhotosIndexComponent,
        PhotoCardComponent,
        UploadPhotoComponent,
        DeletePhotoComponent,

        DashboardComponent,
        GeneralComponent,
        SettingsComponent,
    ],
    imports: [
        BrowserAnimationsModule,
        // SimpleNotificationsModule,

        BrowserModule,
        FormsModule,
        HttpModule,
        RouterModule.forChild(routes),

        MaterialModule,

        SharedModule,
        NotificationsModule,
        FileSubmitModule
    ],
    providers: [
        AuthService,
        AuthResolver,
        GuestResolver,
        HttpServiceRequester,
        PhotosService,
        NotificationsService
    ],
    entryComponents: [
        UploadPhotoComponent,
        DeletePhotoComponent
    ],
    bootstrap: []
})
export class AdminModule {
}
