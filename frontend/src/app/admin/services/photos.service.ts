import {Injectable} from '@angular/core';
import {HttpServiceRequester} from '../../shared/requesters/http.service.requester';
import {Photo} from "../../shared/models/photo.model";

@Injectable()
export class PhotosService {
    constructor(protected requester: HttpServiceRequester) {

    }

    list(): Promise<Photo[]> {
        return this.requester.makeRequest(
            '/admin/photos',
            'GET'
        );
    }

    createPhoto(data: FormData): Promise<Photo> {
        return this.requester.submitFormData(
            '/admin/photos/',
            'POST',
            data
        );
    }

    updatePhoto(photoId: number, data: FormData): Promise<Photo> {
        return this.requester.submitFormData(
            '/admin/photos/' + photoId,
            'PUT',
            data
        );
    }

    deletePhoto(id: number): Promise<boolean> {
        return this.requester.makeRequest(
            '/admin/photos/' + id,
            'DELETE'
        );
    }
}
