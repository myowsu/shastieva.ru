import {Inject, Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Http} from '@angular/http';
import {AuthService} from '../auth.service';
import ErrorsNumbers from "../../../../errors/errors-numbers";


@Injectable()
export class AuthResolver implements Resolve<{}> {
    constructor(protected Http: Http, @Inject(AuthService) protected authService: AuthService, protected router: Router) {
    }

    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot) {

        return this.load();
    }

    load() {
        return this.authService.getAuthUser()
            .catch(err => {
                // todo find way to catch specific error types in typescript
                if (err.code === ErrorsNumbers.UNAUTHORIZED_REQUEST_ERROR) {
                    this.router.navigate(['/admin', 'auth', 'login']);

                    return true;
                }

                throw err;
            });
    }
}
