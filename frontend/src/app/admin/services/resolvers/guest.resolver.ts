import {Inject, Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Http} from '@angular/http';
import {AuthService} from '../auth.service';
import ErrorsNumbers from "../../../../errors/errors-numbers";

@Injectable()
export class GuestResolver implements Resolve<{}> {
    constructor(protected Http: Http, @Inject(AuthService) protected authService: AuthService, protected router: Router) {
    }

    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot) {

        return this.load();
    }

    load() {
        return this.authService.getAuthUser()
            .then(user => {
                if (user) {
                    this.router.navigate(['/admin']);
                }

                return true;
            })
            .catch(err => {
                if (err.code === ErrorsNumbers.UNAUTHORIZED_REQUEST_ERROR) {
                    // it's ok for quest resolving /me endpoint
                    return true;
                }

                throw err;
            })
    }
}
