import {Injectable} from '@angular/core';
import {HttpServiceRequester} from '../../shared/requesters/http.service.requester';
import {User} from '../models';

@Injectable()
export class AuthService {
    public authUser: User;

    constructor(protected requester: HttpServiceRequester) {

    }

    public getAuthUser(): Promise<User> {
        return new Promise((resolve, reject) => {
            if (this.authUser) {
                return resolve(this.authUser);
            }

            return this.getMe()
                .then(authUser => {
                    this.authUser = authUser;

                    return resolve(this.authUser);
                })
                .catch(reject);
        });
    }

    getMe(): Promise<User> {
        return this.requester.makeRequest(
            '/auth/me',
            'GET'
        );
    }

    login(email, password) {
        return this.requester.makeRequest(
            '/auth/login',
            'POST',
            {
                email,
                password
            }
        );
    }

    logout(): Promise<boolean> {
        return this.requester.makeRequest(
            '/auth/logout',
            'DELETE'
        )
            .then(() => {
                this.authUser = null;

                return true;
            })
    }
}
