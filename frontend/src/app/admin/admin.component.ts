import {Component} from '@angular/core';
import {AuthService} from "./services/auth.service";
import {Router} from "@angular/router";

@Component({
    selector: 'app-admin',
    templateUrl: './admin.component.html',
    styleUrls: ['./admin.component.css']
})
export class AdminComponent {
    protected notificationsOptions = {
        pauseOnHover: true,
        clickToClose: true,
        timeOut: 5000
    };

    constructor(protected authService: AuthService, protected router: Router) {
    }

    doLogout() {
        return this.authService.logout()
            .then(() => {

                console.log('router navigate');

                this.router.navigate(['/admin', 'auth', 'login']);

                return true;
            });
    }
}
