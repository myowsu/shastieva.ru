import DateTimeFormat = Intl.DateTimeFormat;

export class BaseModel {
    created_at: DateTimeFormat;
    updated_at: DateTimeFormat;
    deleted_at: DateTimeFormat;
    id: number;
}

export class User extends BaseModel {
    email: string;
    password: string;
}
