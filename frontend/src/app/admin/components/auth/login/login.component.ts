import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../../services/auth.service';

@Component({
    selector: 'app-auth-login',
    templateUrl: 'login.component.html',
    styleUrls: [
        'login.component.css'
    ]
})
export class LoginComponent {
    protected model = {
        email: '',
        password: ''
    };

    constructor(protected authService: AuthService, protected router: Router) {

    }

    login() {
        return this.authService.login(
            this.model.email,
            this.model.password
        )
            .then(() => {
                return this.router.navigate(['/admin']);
            });
    }
}
