import {Component} from '@angular/core';
import {Router} from '@angular/router';

@Component({
    selector: 'app-admin-general',
    templateUrl: 'general.component.html',
    styleUrls: [
        'general.component.scss'
    ]
})
export class GeneralComponent {
    constructor(protected router: Router) {

    }
}
