import {Component, OnInit} from '@angular/core';
import {UploadPhotoComponent} from "../upload-photo/upload-photo.component";
import {MdDialog, MdSelectChange} from "@angular/material";
import {PhotosService} from "../../../../services/photos.service";
import {CATEGORIES_LIST, Photo} from "../../../../../shared/models/photo.model";

@Component({
    selector: 'app-photos-index',
    templateUrl: 'index.component.html',
    styleUrls: [
        'index.component.scss'
    ]
})
export class PhotosIndexComponent implements OnInit {
    protected photos: Photo[];
    protected filteredPhotos: Photo[];

    protected categoriesList = CATEGORIES_LIST;

    protected filter = {
        category: CATEGORIES_LIST
    };

    constructor(public dialog: MdDialog, protected photosService: PhotosService) {

    }

    ngOnInit() {
        return this.getPhotosList();
    }

    protected openUploadDialog() {
        let dialogRef = this.dialog.open(UploadPhotoComponent);

        dialogRef.afterClosed()
            .subscribe(createdPhoto => {
                if (!createdPhoto) {
                    return;
                }

                // add newly created photo to the top of the list
                this.photos.unshift(
                    createdPhoto
                );

                this.filterPhotosList();
            });
    }

    getPhotosList() {
        return this.photosService.list()
            .then(photos => {
                this.photos = photos;

                this.filterPhotosList();
            });
    }

    onPhotoDeleted(photo) {
        this.photos.forEach((photoFromList, index) => {
            if (photo.id === photoFromList.id) {
                this.photos.splice(index, 1);

                this.filterPhotosList();
            }
        });
    }

    onPhotoUpdated(photo) {
        this.photos = this.photos.map((photoFromList, index) => {
            if (photo.id === photoFromList.id) {
                return photo;
            }

            return photoFromList;
        });

        this.filterPhotosList();
    }

    filterPhotosList() {
        this.filteredPhotos = [];

        this.filteredPhotos = this.photos.filter(photo => {
            let valid = true;

            if (this.filter.category.indexOf(photo.category) === -1) {
                valid = false;
            }

            return valid;
        })
    }
}
