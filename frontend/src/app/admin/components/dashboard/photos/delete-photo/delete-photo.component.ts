import {Component, Input} from '@angular/core';
import {PhotosService} from "../../../../services/photos.service";
import {MdDialogRef} from "@angular/material";
import {Photo} from "../../../../../shared/models/photo.model";

@Component({
    selector: 'app-delete-photo',
    templateUrl: 'delete-photo.component.html',
    styleUrls: [
        'delete-photo.component.scss'
    ]
})
export class DeletePhotoComponent {
    @Input() photo: Photo;

    constructor(public dialogRef: MdDialogRef<DeletePhotoComponent>,
                protected photosService: PhotosService) {

    }


    protected deletePhoto() {
        return this.photosService.deletePhoto(this.photo.id)
            .then(() => {
                this.dialogRef.close(this.photo);
            })
    }

    protected cancel() {
        return this.dialogRef.close(false);
    }
}
