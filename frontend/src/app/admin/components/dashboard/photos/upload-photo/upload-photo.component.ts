import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {MdDialogRef} from "@angular/material";
import {PhotosService} from "../../../../services/photos.service";
import {Photo, CATEGORIES_LIST} from "../../../../../shared/models/photo.model";
import {FileSubmitDirective} from 'angular4-file-submit';
import {NotificationsService} from "angular4-notify";

@Component({
    selector: 'app-upload-photo',
    templateUrl: 'upload-photo.component.html',
    styleUrls: [
        'upload-photo.component.scss'
    ]
})
export class UploadPhotoComponent implements OnInit {
    @Input() existingPhoto: Photo;
    @ViewChild(FileSubmitDirective) fileSubmitDirective: FileSubmitDirective;

    protected categoriesList = CATEGORIES_LIST;

    protected file;

    protected model = {
        name: '',
        description: '',
        category: '',
    };

    constructor(public dialogRef: MdDialogRef<UploadPhotoComponent>,
                protected notificationsService: NotificationsService,
                protected photosService: PhotosService) {
    }

    ngOnInit() {
        if (!this.existingPhoto) {
            return;
        }

        for (const prop in this.model) {
            if (this.model.hasOwnProperty(prop) && this.existingPhoto[prop]) {
                this.model[prop] = this.existingPhoto[prop];
            }
        }
    }

    validateModel(): boolean {
        let isValid = true;

        let requiredPropsList = [
            'name',
            'category',
        ];

        requiredPropsList.forEach(requiredProp => {
            if (!this.model[requiredProp]) {
                this.notificationsService.addError(`${requiredProp} is required`);

                isValid = false;

                return false;
            }
        });

        return isValid;
    }

    uploadPhoto() {
        if (!this.validateModel()) {
            return;
        }

        let formData = this.fileSubmitDirective.getFormData();

        for (const prop in this.model) {
            formData.set(prop, this.model[prop]);
        }


        let promises = [];

        if (!this.existingPhoto) {
            promises.push(
                this.photosService.createPhoto(formData)
            );
        } else {
            promises.push(
                this.photosService.updatePhoto(this.existingPhoto.id, formData)
            );
        }

        return Promise.all(promises)
            .then((results) => {
                this.dialogRef.close(results[0]);
            });
    }
}
