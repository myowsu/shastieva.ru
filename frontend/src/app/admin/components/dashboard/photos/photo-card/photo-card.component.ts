import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Photo} from "../../../../../shared/models/photo.model";
import {MdDialog} from "@angular/material";
import {DeletePhotoComponent} from "../delete-photo/delete-photo.component";
import {UploadPhotoComponent} from "../upload-photo/upload-photo.component";

@Component({
    selector: 'app-photo-card',
    templateUrl: 'photo-card.component.html',
    styleUrls: [
        'photo-card.component.scss'
    ]
})
export class PhotoCardComponent {
    @Input() photo: Photo;

    @Output() photoDeleted = new EventEmitter();
    @Output() photoUpdated = new EventEmitter();
    @Output() photoCreated = new EventEmitter();

    constructor(public dialog: MdDialog) {

    }

    protected openDeleteDialog() {
        let dialogRef = this.dialog.open(DeletePhotoComponent);
        dialogRef.componentInstance.photo = this.photo;

        dialogRef.afterClosed()
            .subscribe(deletedPhoto => {
                if (deletedPhoto) {
                    this.photoDeleted.emit(deletedPhoto);
                }
            });
    }

    getCategoryShort(category) {
        let aliasObj = {
            bw: 'BW',
            colour: 'CLR',
            mobile: 'MB'
        };

        return aliasObj[category];
    }

    editPhoto() {
        let dialogRef = this.dialog.open(UploadPhotoComponent);
        dialogRef.componentInstance.existingPhoto = this.photo;

        dialogRef
            .afterClosed()
            .subscribe(updatedPhoto => {
                if (updatedPhoto) {
                    this.photoUpdated.emit(updatedPhoto);
                }
            });
    }
}
