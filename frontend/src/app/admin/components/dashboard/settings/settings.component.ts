import {Component} from '@angular/core';
import {Router} from '@angular/router';

@Component({
    selector: 'app-admin-settings',
    templateUrl: 'settings.component.html',
    styleUrls: [
        'settings.component.scss'
    ]
})
export class SettingsComponent {
    constructor(protected router: Router) {

    }
}
