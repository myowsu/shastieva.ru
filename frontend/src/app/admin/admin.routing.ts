import {Routes} from '@angular/router';
import {AuthResolver} from './services/resolvers/auth.resolver';
import {LoginComponent} from './components/auth/login/login.component';
import {AdminComponent} from './admin.component';
import {GuestResolver} from "./services/resolvers/guest.resolver";
import {DashboardComponent} from "./components/dashboard/dashboard.component";
import {PhotosIndexComponent} from "./components/dashboard/photos/index/index.component";
import {GeneralComponent} from "./components/dashboard/general/general.component";
import {SettingsComponent} from "./components/dashboard/settings/settings.component";

export const routes: Routes = [
    {
        path: 'admin',
        component: AdminComponent,
        children: [
            {
                path: '',
                redirectTo: 'dashboard',
                pathMatch: 'full'
            },

            {
                path: 'auth',
                resolve: {
                    guest: GuestResolver
                },
                children: [
                    {
                        path: 'login',
                        component: LoginComponent
                    },
                ]
            },

            {
                path: 'dashboard',
                resolve: {
                    auth: AuthResolver,
                },
                component: DashboardComponent,
                children: [
                    {
                        path: '',
                        redirectTo: 'general',
                        pathMatch: 'full'
                    },
                    {
                        path: 'photos',
                        component: PhotosIndexComponent
                    },
                    {
                        path: 'general',
                        component: GeneralComponent
                    },
                    {
                        path: 'settings',
                        component: SettingsComponent
                    },

                ]
            }
        ]
    }
];