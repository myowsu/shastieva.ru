export class BaseError extends Error {
    protected code = 0;

    constructor(message, codeNum) {
        super(message);

        this.code = codeNum;
    }
}