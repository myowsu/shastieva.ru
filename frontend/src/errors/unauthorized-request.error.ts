import {BaseError} from "./base.error";
import ErrorsNumbers from "./errors-numbers";

export class UnauthorizedRequestError extends BaseError {
    constructor(message) {
        super(message, ErrorsNumbers.UNAUTHORIZED_REQUEST_ERROR);
    }
}