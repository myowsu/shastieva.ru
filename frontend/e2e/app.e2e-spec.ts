import { ShastievaPage } from './app.po';

describe('shastieva App', () => {
  let page: ShastievaPage;

  beforeEach(() => {
    page = new ShastievaPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
